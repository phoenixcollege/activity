<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Viewer;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\App\DuskTestCase;

/**
 * @package Tests\App\Browser
 *
 * *NOTE* if you session disappears between tests, check for
 * SESSION_SECURE_COOKIE set to false.  Dusk tests default to http,
 * not https
 */
class AdminManagerTest extends DuskTestCase
{

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUser(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/managers')
                    ->assertSee('Manager Administration')
                    ->assertSeeLink($user->id);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithoutAdminUser(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/managers')
                    ->assertSee("You don't have permission");
        });
    }

    public function testIndexCanSeeUsersWithManageRole(): void
    {
        $this->browse(function (Browser $browser) {
            $user1 = User::factory([
                'id' => 1,
                'username' => 'foobar',
                'email' => 'foobar@example.org',
                'first_name' => 'foo',
                'last_name' => 'bar',
            ])->create();
            $user2 = User::factory([
                'id' => 2,
                'username' => 'foobar2',
                'email' => 'foobar2@example.org',
                'first_name' => 'foo2',
                'last_name' => 'bar2',
            ])->create();
            RoleUser::create(['user_id' => $user2->id, 'role_id' => 3]);
            $user3 = User::factory([
                'id' => 3,
                'username' => 'foobar3',
                'email' => 'foobar3@example.org',
                'first_name' => 'foo3',
                'last_name' => 'bar3',
            ])->create();
            $browser->loginAs($user1)
                    ->visit('/admin/managers')
                    ->assertSee('Manager Administration')
                    ->assertSeeLink($user2->id)
                    ->assertDontSeeLink($user3->id);
        });
    }

    public function testViewCanChangeUserStatus(): void
    {
        $this->browse(function (Browser $browser) {
            $user1 = User::factory([
                'id' => 1,
                'username' => 'foobar',
                'email' => 'foobar@example.org',
                'first_name' => 'foo',
                'last_name' => 'bar',
            ])->create();
            $user2 = User::factory([
                'id' => 2,
                'username' => 'foobar2',
                'email' => 'foobar2@example.org',
                'first_name' => 'foo2',
                'last_name' => 'bar2',
            ])->create();
            RoleUser::create(['user_id' => $user2->id, 'role_id' => 3]);
            $user3 = User::factory([
                'id' => 3,
                'username' => 'foobar3',
                'email' => 'foobar3@example.org',
                'first_name' => 'foo3',
                'last_name' => 'bar3',
            ])->create();
            $browser->loginAs($user1)
                    ->visit('/admin/managers/view/'.$user2->id)
                    ->assertSee('Manager Administration')
                    ->assertSee($user1->id)
                    ->assertSee($user2->id)
                    ->assertSee($user3->id)
                    ->assertNotChecked('viewer['.$user1->id.']')
                    ->assertNotChecked('viewer['.$user2->id.']')
                    ->assertNotChecked('viewer['.$user3->id.']')
                    ->check('viewer['.$user3->id.']');
            $browser->waitFor('.flash', 1);
            $browser->waitUntilMissing('.flash', 2);
        });
    }

    public function testViewCanSeeManagedUsers(): void
    {
        $this->browse(function (Browser $browser) {
            $user1 = User::factory([
                'id' => 1,
                'username' => 'foobar',
                'email' => 'foobar@example.org',
                'first_name' => 'foo',
                'last_name' => 'bar',
            ])->create();
            $user2 = User::factory([
                'id' => 2,
                'username' => 'foobar2',
                'email' => 'foobar2@example.org',
                'first_name' => 'foo2',
                'last_name' => 'bar2',
            ])->create();
            RoleUser::create(['user_id' => $user2->id, 'role_id' => 3]);
            $user3 = User::factory([
                'id' => 3,
                'username' => 'foobar3',
                'email' => 'foobar3@example.org',
                'first_name' => 'foo3',
                'last_name' => 'bar3',
            ])->create();
            Viewer::factory(['viewer_user_id' => $user2->id, 'user_id' => $user3->id])->create();
            $browser->loginAs($user1)
                    ->visit('/admin/managers/view/'.$user2->id)
                    ->assertSee('Manager Administration')
                    ->assertSee($user1->id)
                    ->assertSee($user2->id)
                    ->assertSee($user3->id)
                    ->assertNotChecked('viewer['.$user1->id.']')
                    ->assertNotChecked('viewer['.$user2->id.']')
                    ->assertChecked('viewer['.$user3->id.']');
        });
    }

    public function testViewSeeUsers(): void
    {
        $this->browse(function (Browser $browser) {
            $user1 = User::factory([
                'id' => 1,
                'username' => 'foobar',
                'email' => 'foobar@example.org',
                'first_name' => 'foo',
                'last_name' => 'bar',
            ])->create();
            $user2 = User::factory([
                'id' => 2,
                'username' => 'foobar2',
                'email' => 'foobar2@example.org',
                'first_name' => 'foo2',
                'last_name' => 'bar2',
            ])->create();
            RoleUser::create(['user_id' => $user2->id, 'role_id' => 3]);
            $user3 = User::factory([
                'id' => 3,
                'username' => 'foobar3',
                'email' => 'foobar3@example.org',
                'first_name' => 'foo3',
                'last_name' => 'bar3',
            ])->create();
            $browser->loginAs($user1)
                    ->visit('/admin/managers/view/'.$user2->id)
                    ->assertSee('Manager Administration')
                    ->assertSee($user1->id)
                    ->assertSee($user2->id)
                    ->assertSee($user3->id)
                    ->assertNotChecked('viewer['.$user1->id.']')
                    ->assertNotChecked('viewer['.$user2->id.']')
                    ->assertNotChecked('viewer['.$user3->id.']');
        });
    }
}
