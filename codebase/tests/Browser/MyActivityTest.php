<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Activity;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;

/**
 * @package Tests\App\Browser
 *
 * *NOTE* if you session disappears between tests, check for
 * SESSION_SECURE_COOKIE set to false.  Dusk tests default to http,
 * not https
 */
class MyActivityTest extends DuskTestCase
{

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRoute(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/user')
                    ->assertSee('My Activity')
                    ->assertSee('No records found');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecord(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/user')
                    ->clickLink('New')
                    ->assertPathIs('/user/create')
                    ->type('description', 'Test Create')
                    ->type('time_spent', 5)
                    ->press('Save')
                    ->assertPathIs('/user')
                    ->assertSee('Test Create');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecordFailsValidation(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/user')
                    ->clickLink('New')
                    ->assertPathIs('/user/create')
                    ->press('Save')
                    ->assertPathIs('/user/create')
                    ->assertSee('description field is required')
                    ->assertDontSee('user id field is required');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteExistingRecord(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $model = Activity::factory(['user_id' => $user->id])->create();
            $browser->loginAs($user)
                    ->visit('/user')
                    ->assertSee($model->description)
                    ->clickLink('delete')
                    ->assertPathIs('/user/delete/'.$model->id)
                    ->assertSee($model->description)
                    ->press('Delete')
                    ->assertPathIs('/user')
                    ->assertDontSee($model->description);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteMissingRecord(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/user/delete/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteNotOwner(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $model = Activity::factory(['user_id' => 99])->create();
            $browser->loginAs($user)
                    ->visit('/user/delete/'.$model->id)
                    ->assertSee("You don't have permission");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateExistingRecord(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $model = Activity::factory(['user_id' => $user->id])->create();
            $browser->loginAs($user)
                    ->visit('/user')
                    ->assertSee($model->description)
                    ->clickLink('update')
                    ->assertPathIs('/user/update/'.$model->id)
                    ->assertInputValue('description', $model->description)
                    ->assertInputValue('started_at', $model->started_at->toDateTimeLocalString())
                    ->assertInputValue('time_spent', $model->time_spent)
                    ->type('description', 'Test Update')
                    ->press('Save')
                    ->assertPathIs('/user')
                    ->assertSee('Test Update');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateExistingRecordFailsValidation(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $model = Activity::factory(['user_id' => $user->id])->create();
            $browser->loginAs($user)
                    ->visit('/user')
                    ->assertSee($model->description)
                    ->clickLink('update')
                    ->assertPathIs('/user/update/'.$model->id)
                    ->assertInputValue('description', $model->description)
                    ->assertInputValue('started_at', $model->started_at->toDateTimeLocalString())
                    ->assertInputValue('time_spent', $model->time_spent)
                    ->clear('description')
                    ->press('Save')
                    ->assertPathIs('/user/update/'.$model->id)
                    ->assertSee('description field is required');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateMissingRecord(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/user/update/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateNotOwner(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $model = Activity::factory(['user_id' => 99])->create();
            $browser->loginAs($user)
                    ->visit('/user/update/'.$model->id)
                    ->assertSee("You don't have permission");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewExistingRecord(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $model = Activity::factory(['user_id' => $user->id])->create();
            $browser->loginAs($user)
                    ->visit('/user')
                    ->assertSee($model->description)
                    ->clickLink($model->id)
                    ->assertPathIs('/user/view/'.$model->id)
                    ->assertSee($model->description);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewMissingRecord(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/user/view/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewNotOwner(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $model = Activity::factory(['user_id' => 99])->create();
            $browser->loginAs($user)
                    ->visit('/user/view/'.$model->id)
                    ->assertSee("You don't have permission");
        });
    }
}
