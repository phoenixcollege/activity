<?php

namespace Tests\App\Browser;

use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class HomeTest extends DuskTestCase
{

    public function testBaseRouteWithoutAuthenticatedUser(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertPathIs('/login');
        });
    }

    public function testHomeRouteAddDefaultValues(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/')
                    ->type('description', 'I did stuff')
                    ->press('Add')
                    ->waitForText('I did stuff')
                    ->assertSee('I did stuff');
        });
    }

    public function testHomeRouteAddValidationError(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/')
                    ->press('Add')
                    ->waitForText('description field is required')
                    ->assertSee('description field is required');
        });
    }

    public function testHomeRouteAddWithTimeSpent(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/')
                    ->type('description', 'I did stuff')
                    ->type('time_spent', 99)
                    ->press('Add')
                    ->waitForText('I did stuff')
                    ->assertSee('for 99 minutes');
        });
    }
}
