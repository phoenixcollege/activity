<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Activity;
use Carbon\Carbon;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;
use Tests\App\Traits\EnsureDatabaseFile;

/**
 * @package Tests\App\Browser
 *
 * *NOTE* if you session disappears between tests, check for
 * SESSION_SECURE_COOKIE set to false.  Dusk tests default to http,
 * not https
 */
class AdminActivityTest extends DuskTestCase
{

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUser(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/activity')
                    ->assertSee('Activity Administration')
                    ->assertSee('No records found');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithoutAdminUser(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/activity')
                    ->assertSee("You don't have permission");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecord(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/activity')
                    ->clickLink('New')
                    ->assertPathIs('/admin/activity/create')
                    ->type('user_id', '12345')
                    ->type('description', 'Test Create')
                    ->type('time_spent', 5)
                    ->press('Save')
                    ->assertPathIs('/admin/activity')
                    ->assertSee('Test Create');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateNewRecordFailsValidation(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/activity')
                    ->clickLink('New')
                    ->assertPathIs('/admin/activity/create')
                    ->press('Save')
                    ->assertPathIs('/admin/activity/create')
                    ->assertSee('description field is required')
                    ->assertSee('user id field is required');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteExistingRecord(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $model = Activity::factory()->create();
            $browser->loginAs($user)
                    ->visit('/admin/activity')
                    ->assertSee($model->description)
                    ->clickLink('delete')
                    ->assertPathIs('/admin/activity/delete/'.$model->id)
                    ->assertSee($model->description)
                    ->press('Delete')
                    ->assertPathIs('/admin/activity')
                    ->assertDontSee($model->description);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteMissingRecord(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/activity/delete/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateExistingRecord(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $model = Activity::factory()->create();
            $browser->loginAs($user)
                    ->visit('/admin/activity')
                    ->assertSee($model->description)
                    ->clickLink('update')
                    ->assertPathIs('/admin/activity/update/'.$model->id)
                    ->assertInputValue('description', $model->description)
                    ->assertInputValue('user_id', $model->user_id)
                    ->assertInputValue('started_at', $model->started_at->toDateTimeLocalString())
                    ->assertInputValue('time_spent', $model->time_spent)
                    ->type('description', 'Test Update')
                    ->press('Save')
                    ->assertPathIs('/admin/activity')
                    ->assertSee('Test Update');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateExistingRecordFailsValidation(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $model = Activity::factory()->create();
            $browser->loginAs($user)
                    ->visit('/admin/activity')
                    ->assertSee($model->description)
                    ->clickLink('update')
                    ->assertPathIs('/admin/activity/update/'.$model->id)
                    ->assertInputValue('description', $model->description)
                    ->assertInputValue('user_id', $model->user_id)
                    ->assertInputValue('started_at', $model->started_at->toDateTimeLocalString())
                    ->assertInputValue('time_spent', $model->time_spent)
                    ->clear('description')
                    ->press('Save')
                    ->assertPathIs('/admin/activity/update/'.$model->id)
                    ->assertSee('description field is required');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateMissingRecord(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/activity/update/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewExistingRecord(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $model = Activity::factory()->create();
            $browser->loginAs($user)
                    ->visit('/admin/activity')
                    ->assertSee($model->description)
                    ->clickLink($model->id)
                    ->assertPathIs('/admin/activity/view/'.$model->id)
                    ->assertSee($model->description);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewMissingRecord(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/admin/activity/view/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }
}
