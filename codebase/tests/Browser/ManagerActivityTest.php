<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Activity;
use App\Models\Eloquent\Viewer;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\App\DuskTestCase;

/**
 * @package Tests\App\Browser
 *
 * *NOTE* if you session disappears between tests, check for
 * SESSION_SECURE_COOKIE set to false.  Dusk tests default to http,
 * not https
 */
class ManagerActivityTest extends DuskTestCase
{

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithManageUser(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        RoleUser::create(['user_id' => $user->id, 'role_id' => 3]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/manage/activity')
                    ->assertSee('Manage Activity')
                    ->assertSee('No records found');
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testBaseRouteWithoutManageUser(): void
    {
        $user = User::factory([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/manage/activity')
                    ->assertSee("You don't have permission");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewExistingRecord(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $user2 = User::factory()->create();
            $viewer = Viewer::factory(['user_id' => $user2->id, 'viewer_user_id' => $user->id])->create();
            $model = Activity::factory(['user_id' => $user2->id])->create();
            $browser->loginAs($user)
                    ->visit('/manage/activity')
                    ->assertSee($model->description)
                    ->clickLink($model->id)
                    ->assertPathIs('/manage/activity/view/'.$model->id)
                    ->assertSee($model->description);
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewMissingRecord(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/manage/activity/view/2')
                    ->assertSee("The resource you were trying to reach");
        });
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testViewNotManager(): void
    {
        $user = User::factory([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ])->create();
        $this->browse(function (Browser $browser) use ($user) {
            $user2 = User::factory()->create();
            $viewer = Viewer::factory(['user_id' => $user2->id, 'viewer_user_id' => 99])->create();
            $model = Activity::factory(['user_id' => $user2->id])->create();
            $browser->loginAs($user)
                    ->visit('/manage/activity/view/'.$model->id)
                    ->assertSee("You don't have permission");
        });
    }
}
