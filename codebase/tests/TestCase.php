<?php

namespace Tests\App;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\App\Traits\Auth;
use Tests\App\Traits\CreatesApplication;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, Auth;
}
