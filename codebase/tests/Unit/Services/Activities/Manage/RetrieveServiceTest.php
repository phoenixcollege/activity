<?php

namespace Tests\App\Unit\Services\Activities\Manage;

use App\Contracts\Services\Activities\Manage\RetrieveService;
use App\Contracts\Storage\Activity;
use App\Models\Eloquent\User;
use App\Models\Eloquent\Viewer;
use App\Providers\Services\Manage\ActivityServices;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Gate;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Contracts\Services\GateService;

class RetrieveServiceTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCanFailGateCheck(): void
    {
        $sut = $this->getSut();
        $model = new \App\Models\Eloquent\Activity(['user_id' => '55555']);
        $viewers = new Collection([
            new Viewer(['viewer_user_id' => '99999', 'user_id' => '55555']),
        ]);
        $model->setRelation('viewers', $viewers);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $request = new Request();
        $request->setUserResolver($this->getUserResolver());
        $this->expectException(AuthorizationException::class);
        $activityResult = $sut->findById(1);
    }

    public function testCanPassGateCheck(): void
    {
        $sut = $this->getSut();
        $model = new \App\Models\Eloquent\Activity(['user_id' => '55555']);
        $viewers = new Collection([
            new Viewer(['viewer_user_id' => '12345', 'user_id' => '55555']),
        ]);
        $model->setRelation('viewers', $viewers);
        $sut->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $request = new Request();
        $request->setUserResolver($this->getUserResolver());
        $activityResult = $sut->findById(1);
        $this->assertSame($model, $activityResult->model);
    }

    protected function getSut(): RetrieveService
    {
        $provider = m::mock(Activity::class);
        $provider->shouldReceive('getModel')->andReturn(new \App\Models\Eloquent\Activity());
        $gate = new Gate(new Container(), $this->getUserResolver());
        ActivityServices::defineGates($gate);
        return new \App\Services\Activities\Manage\RetrieveService(
            $provider,
            [
                GateService::class => new \Smorken\Service\Services\GateService($gate),
            ]
        );
    }

    protected function getUserResolver(string $id = '12345'): callable
    {
        return function () use ($id) {
            return new User(['id' => $id]);
        };
    }
}
