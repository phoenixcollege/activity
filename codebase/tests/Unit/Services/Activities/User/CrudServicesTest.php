<?php

namespace Tests\App\Unit\Services\Activities\User;

use App\Contracts\Services\Activities\User\CrudServices;
use App\Contracts\Storage\Activity;
use App\Models\Eloquent\User;
use App\Providers\Services\User\ActivityServices;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Gate;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\GateService;
use Smorken\Service\Contracts\Services\RequestService;
use Smorken\Service\Contracts\Services\ValidatorService;

class CrudServicesTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCreate(): void
    {
        $sut = $this->getSut();
        $sut->getCreateService()->getProvider()->shouldReceive('validationRules')
            ->andReturn(
                [
                    'user_id' => 'required|string|max:32',
                    'description' => 'required|string|max:255',
                    'started_at' => 'date',
                    'time_spent' => 'int|max:999',
                ]
            );
        $model = new \App\Models\Eloquent\Activity();
        $sut->getCreateService()->getProvider()->shouldReceive('create')
            ->once()
            ->with(m::type('array'))
            ->andReturnUsing(function ($attributes) use ($model) {
                $expected = [
                    'description' => 'foo',
                    'user_id' => '12345',
                    'time_spent' => 4,
                ];
                foreach ($expected as $k => $v) {
                    $this->assertEquals($v, $attributes[$k]);
                }
                return $model;
            });
        $request = new Request([
            'description' => 'foo',
            'user_id' => 'abc',
            'started_at' => Carbon::now()->toDateTimeLocalString(),
            'time_spent' => 4,
        ]);
        $request->setUserResolver(function () {
            return new User(['id' => '12345']);
        });
        $activityResult = $sut->getCreateService()->createFromRequest($request);
        $this->assertSame($model, $activityResult->model);
        $this->assertEmpty($activityResult->messageBag);
    }

    public function testUpdateCanFailGateCheck(): void
    {
        $sut = $this->getSut();
        $sut->getCreateService()->getProvider()->shouldReceive('validationRules')
            ->andReturn(
                [
                    'user_id' => 'required|string|max:32',
                    'description' => 'required|string|max:255',
                    'started_at' => 'date',
                    'time_spent' => 'int|max:999',
                ]
            );
        $model = new \App\Models\Eloquent\Activity(['user_id' => '55555']);
        $sut->getUpdateService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $sut->getUpdateService()->getProvider()->shouldReceive('update')
            ->never();
        $request = new Request([
            'description' => 'foo',
            'user_id' => 'abc',
            'started_at' => Carbon::now()->toDateTimeLocalString(),
            'time_spent' => 4,
        ]);
        $request->setUserResolver(function () {
            return new User(['id' => '12345']);
        });
        $this->expectException(AuthorizationException::class);
        $activityResult = $sut->getUpdateService()->updateFromRequest($request, 1);
    }

    public function testUpdateCanPassGateCheck(): void
    {
        $sut = $this->getSut();
        $sut->getCreateService()->getProvider()->shouldReceive('validationRules')
            ->andReturn(
                [
                    'user_id' => 'required|string|max:32',
                    'description' => 'required|string|max:255',
                    'started_at' => 'date',
                    'time_spent' => 'int|max:999',
                ]
            );
        $model = new \App\Models\Eloquent\Activity(['user_id' => '12345']);
        $sut->getUpdateService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $sut->getUpdateService()->getProvider()->shouldReceive('update')
            ->once()
            ->with($model, m::type('array'))
            ->andReturnUsing(function ($m, $attributes) use ($model) {
                $expected = [
                    'description' => 'foo',
                    'user_id' => '12345',
                    'time_spent' => 4,
                ];
                foreach ($expected as $k => $v) {
                    $this->assertEquals($v, $attributes[$k]);
                }
                return $model;
            });
        $request = new Request([
            'description' => 'foo',
            'user_id' => 'abc',
            'started_at' => Carbon::now()->toDateTimeLocalString(),
            'time_spent' => 4,
        ]);
        $request->setUserResolver(function () {
            return new User(['id' => '12345']);
        });
        $activityResult = $sut->getUpdateService()->updateFromRequest($request, 1);
        $this->assertSame($model, $activityResult->model);
        $this->assertEmpty($activityResult->messageBag);
    }

    public function testViewCanFailGateCheck(): void
    {
        $sut = $this->getSut();
        $model = new \App\Models\Eloquent\Activity(['user_id' => '55555']);
        $sut->getRetrieveService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $request = new Request([
            'description' => 'foo',
            'user_id' => 'abc',
            'started_at' => Carbon::now()->toDateTimeLocalString(),
            'time_spent' => 4,
        ]);
        $request->setUserResolver(function () {
            return new User(['id' => '12345']);
        });
        $this->expectException(AuthorizationException::class);
        $activityResult = $sut->getRetrieveService()->findById(1);
    }

    public function testViewCanPassGateCheck(): void
    {
        $sut = $this->getSut();
        $model = new \App\Models\Eloquent\Activity(['user_id' => '12345']);
        $sut->getRetrieveService()->getProvider()->shouldReceive('findOrFail')
            ->once()
            ->with(1)
            ->andReturn($model);
        $request = new Request([
            'description' => 'foo',
            'user_id' => 'abc',
            'started_at' => Carbon::now()->toDateTimeLocalString(),
            'time_spent' => 4,
        ]);
        $request->setUserResolver(function () {
            return new User(['id' => '12345']);
        });
        $activityResult = $sut->getRetrieveService()->findById(1);
        $this->assertSame($model, $activityResult->model);
        $this->assertEmpty($activityResult->messageBag);
    }

    protected function getSut(): CrudServices
    {
        $provider = m::mock(Activity::class);
        $provider->shouldReceive('getModel')->andReturn(new \App\Models\Eloquent\Activity());
        $gate = new Gate(new Container(), function () {
            return new User(['id' => '12345']);
        });
        ActivityServices::defineGates($gate);
        $services = [
            FilterService::class => new \App\Services\Activities\User\FilterService(),
            GateService::class => new \Smorken\Service\Services\GateService($gate),
            RequestService::class => new \App\Services\Activities\User\RequestService(),
            ValidatorService::class => new \Smorken\Service\Services\ValidatorService(new \Illuminate\Validation\Factory(new Translator(new ArrayLoader(),
                'en'))),
        ];
        return \App\Services\Activities\User\CrudServices::createByStorageProvider($provider, $services);
    }
}
