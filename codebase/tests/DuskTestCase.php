<?php

namespace Tests\App;

use Database\Seeders\TestSeeder;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\TestCase as BaseTestCase;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\App\Traits\CreatesApplication;
use Tests\App\Traits\DuskBrowser;
use Tests\App\Traits\DuskEnv;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication, DuskBrowser, DuskEnv, DatabaseMigrations;

    protected string $seeder = TestSeeder::class;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        if (!static::runningInSail()) {
            static::startChromeDriver();
        }
    }

    protected function addRoleForUserId(int $userId = 1, int $roleId = 1): void
    {
        if (class_exists(RoleUser::class)) {
            RoleUser::factory()->create(['user_id' => $userId, 'role_id' => $roleId]);
        }
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return RemoteWebDriver
     * @throws \Exception
     */
    protected function driver()
    {
        $this->checkEndpoints($this->getUrls());
        return $this->getDriver();
    }

    protected function setUp(): void
    {
        $this->initialize();
        parent::setUp();
        $this->addRoleForUserId();
    }
}
