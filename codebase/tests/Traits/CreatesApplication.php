<?php

namespace Tests\App\Traits;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\DB;

trait CreatesApplication
{

    protected bool $checkDatabase = true;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();
        $this->addMigrations($app, $this->getMigrations());
        $this->tryToGetDbConnection();
        return $app;
    }

    protected function addMigrations(Application $app, array $paths = []): void
    {
        $reals = array_map(function ($v) {
            return realpath($v);
        }, $paths);
        if ($reals) {
            $app->afterResolving('migrator', function ($migrator) use ($reals) {
                foreach ($reals as $real) {
                    $migrator->path($real);
                }
            });
        }
    }

    protected function getMigrations(): array
    {
        $migrationsDir = realpath(__DIR__.'/../database/migrations');
        if ($migrationsDir !== false) {
            return [
                $migrationsDir,
            ];
        }
        return [];
    }

    protected function tryToGetDbConnection(): void
    {
        if (!$this->checkDatabase) {
            return;
        }
        for ($i = 0; $i < 10; $i++) {
            try {
                DB::connection()->getPdo();
                return;
            } catch (\Throwable $e) {
                sleep(1);
            }
        }
    }
}
