<?php

namespace Tests\App\Traits;

use Mockery as m;
use Smorken\Auth\Proxy\AuthProvider;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Models\Eloquent\User;

trait Auth
{

    protected function bindMockAuthProvider(bool $detail = true): array
    {
        if ($detail) {
            $provider = m::mock(Provider::class);
            $userProvider = m::mock(\Smorken\Auth\Proxy\Contracts\Storage\User::class);
            $authprovider = new AuthProvider($provider, $userProvider);
        } else {
            $authprovider = m::mock(AuthProvider::class);
        }
        \Illuminate\Support\Facades\Auth::provider('proxy', function ($app, array $config) use ($authprovider) {
            return $authprovider;
        });
        if ($detail) {
            return [$provider, $userProvider];
        }
        return [$authprovider];
    }

    protected function mockAuth(
        $user_data = ['id' => 1, 'first_name' => 'foo', 'last_name' => 'bar', 'email' => 'foomail@example.org'],
        $acting_as = true
    ): \Smorken\Auth\Proxy\Contracts\Models\User {
        $user = User::factory()->make($user_data);
        foreach ($user_data as $k => $v) {
            $user->$k = $v;
        }
        if ($acting_as && method_exists($this, 'actingAs')) {
            $this->actingAs($user);
        }
        return $user;
    }
}
