<?php

namespace Tests\App\Traits;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;

trait DuskBrowser
{
    protected function checkEndpoints(array $urls)
    {
        foreach ($urls as $url) {
            if (!$this->waitForEndpoint($url['url'], $url['callback'])) {
                throw new \Exception($url['url'].' is not responding.');
            }
        }
    }

    protected function getAppUrl(): string
    {
        return $_ENV['APP_URL'] ?? 'http://web:8000';
    }

    protected function getDriver(): RemoteWebDriver
    {
        $url = $this->getDuskDriverUrl();
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless',
            '--window-size=1920,1080',
            '--ignore-ssl-errors',
            '--no-sandbox',
            '--whitelisted-ips',
        ]);
        return RemoteWebDriver::create($url, DesiredCapabilities::chrome()
                                                                ->setCapability(ChromeOptions::CAPABILITY_W3C, $options)
                                                                ->setCapability(WebDriverCapabilityType::ACCEPT_SSL_CERTS,
                                                                    true)
                                                                ->setCapability('acceptInsecureCerts', true));
    }

    protected function getDuskDriverUrl(): string
    {
        return $_ENV['DUSK_DRIVER_URL'] ?? 'http://localhost:9515';
    }

    protected function getResponse(string $url): string|bool
    {
        $c = curl_init();
        curl_setopt_array($c, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
        ]);
        $r = curl_exec($c);
        curl_close($c);
        return $r;
    }

    protected function getUrls(): array
    {
        return [
            [
                'url' => $this->getDuskDriverUrl().'/status',
                'callback' => function ($r) {
                    $response = json_decode($r, true);
                    if ($response && isset($response['value']['ready']) && $response['value']['ready'] === true) {
                        return true;
                    }
                    return false;
                },
            ],
            [
                'url' => $this->getAppUrl(),
                'callback' => function ($r) {
                    return $r !== false;
                },
            ],
        ];
    }

    protected function waitForEndpoint($url, callable $valid)
    {
        for ($i = 0; $i < 5; $i++) {
            $r = $this->getResponse($url);
            if ($valid($r)) {
                return true;
            }
            sleep(1);
        }
        return false;
    }
}
