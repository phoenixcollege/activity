<?php


namespace Tests\App\Traits;


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

trait Table
{
    protected function createTableFromAttributes(
        string $table,
        array $attributes,
        array $additional = [],
        string $connection = null
    ): void {
        $s = Schema::connection($connection);
        if (!$s->hasTable($table)) {
            $s->create($table, function (Blueprint $t) use ($attributes, $additional) {
                foreach ($attributes as $k) {
                    $t->string($k)
                      ->nullable();
                }
                $t->timestamps();
                foreach ($additional as $cmd => $data) {
                    call_user_func_array([$t, $cmd], $data ?? []);
                }
            });
        }
    }

    protected function createTableFromModel(
        object $model,
        array $additional = [],
        string $table = null,
        string $connection = null
    ): void {
        $connection = $connection ?: $model->getConnectionName();
        $table = $table ?: $model->getTable();
        $this->createTableFromAttributes($table, array_keys($model->getAttributes()), $additional, $connection);
    }

    protected function createTableFromModelClass(
        string $model,
        array $additional = [],
        string $table = null,
        string $connection = null
    ): void {
        $m = factory($model)->make();
        $this->createTableFromModel($m, $additional, $table, $connection);
    }

    protected function deleteTable(string $table, string $connection = null)
    {
        $s = Schema::connection($connection);
        $s->dropIfExists($table);
    }

    protected function deleteTableFromModel(object $model, string $table = null, string $connection = null)
    {
        $connection = $connection ?: $model->getConnectionName();
        $table = $table ?: $model->getTable();
        $this->deleteTable($table, $connection);
    }

    protected function deleteTableFromModelClass(string $model, string $table = null, string $connection = null)
    {
        $m = factory($model)->make();
        $this->deleteTableFromModel($m, $table, $connection);
    }
}
