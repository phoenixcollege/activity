<?php


namespace Tests\App\Traits;


trait EnsureDatabaseFile
{

    protected function ensureTestingDatabase($reset = false, $file = null)
    {
        if (is_null($file)) {
            $file = env('DB_DATABASE', '/app/tests/database/testing.sqlite');
        }
        if ($file !== ':memory:' && !file_exists($file)) {
            if (file_exists($file) && $reset) {
                unlink($file);
            }
            if (!file_exists($file)) {
                touch($file);
            }
        }
    }
}
