<?php

namespace Tests\App\Feature\MyActivity;

use App\Models\Eloquent\Activity;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\App\TestCase;

class ViewTest extends TestCase
{

    use RefreshDatabase;

    public function testGateCanAllowAccess(): void
    {
        $user = $this->mockAuth();
        $activity = Activity::factory(['user_id' => 1])->create();
        $response = $this->get('/user/view/'.$activity->id);
        $response->assertStatus(200);
    }

    public function testGateCanBlockAccess(): void
    {
        $user = $this->mockAuth();
        $activity = Activity::factory(['user_id' => 'abc123'])->create();
        $response = $this->get('/user/view/'.$activity->id);
        $response->assertStatus(403);
    }
}
