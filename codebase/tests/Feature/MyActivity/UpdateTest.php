<?php

namespace Tests\App\Feature\MyActivity;

use App\Models\Eloquent\Activity;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\App\TestCase;

class UpdateTest extends TestCase
{

    use RefreshDatabase;

    public function testGateCanAllowAccess(): void
    {
        $user = $this->mockAuth();
        $activity = Activity::factory(['user_id' => 1])->create();
        $response = $this->get('/user/update/'.$activity->id);
        $response->assertStatus(200);
    }

    public function testGateCanBlockAccess(): void
    {
        $user = $this->mockAuth();
        $activity = Activity::factory(['user_id' => 'abc123'])->create();
        $response = $this->get('/user/update/'.$activity->id);
        $response->assertStatus(403);
    }

    public function testGateCanBlockAccessToPost(): void
    {
        $user = $this->mockAuth();
        $activity = Activity::factory(['user_id' => 'abc123'])->create();
        $response = $this->post('/user/update/'.$activity->id, [
            'user_id' => '1',
            'description' => 'foobar',
            'started_at' => Carbon::now()->toDateTimeLocalString(),
            'time_spent' => 0,
        ]);
        $response->assertStatus(403);
    }

    public function testGateCanAllowAccessToPost(): void
    {
        $user = $this->mockAuth();
        $activity = Activity::factory(['user_id' => '1'])->create();
        $response = $this->post('/user/update/'.$activity->id, [
            'description' => 'foobar',
            'started_at' => Carbon::now()->toDateTimeLocalString(),
            'time_spent' => 0,
        ]);
        $response->assertStatus(302);
        $response->assertRedirect('/user?f_userId=1');
    }
}
