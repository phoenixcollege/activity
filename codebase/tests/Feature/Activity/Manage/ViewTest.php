<?php

namespace Tests\App\Feature\Activity\Manage;

use App\Models\Eloquent\Activity;
use App\Models\Eloquent\Viewer;
use Database\Seeders\Smorken\Roles\Models\Eloquent\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Smorken\Roles\Models\Eloquent\Role;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Smorken\Roles\ServiceProvider;
use Tests\App\TestCase;

class ViewTest extends TestCase
{

    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed(RoleSeeder::class);
        ServiceProvider::defineGates($this->app, true);
    }

    public function testUserCanVisit(): void
    {
        $user = $this->mockAuth();
        $this->addRoleToUser($user->id, 1);
        $activity = Activity::factory(['user_id' => 'abc123'])->create();
        $viewer = Viewer::factory(['user_id' => 'abc123', 'viewer_user_id' => $user->id])->create();
        $response = $this->get('/manage/activity');
        $response->assertStatus(200);
    }

    public function testGateCanAllowAccess(): void
    {
        $user = $this->mockAuth();
        $this->addRoleToUser($user->id, 1);
        $activity = Activity::factory(['user_id' => 'abc123'])->create();
        $viewer = Viewer::factory(['user_id' => 'abc123', 'viewer_user_id' => $user->id])->create();
        $response = $this->get('/manage/activity/view/'.$activity->id);
        $response->assertStatus(200);
    }

    public function testGateCanBlockAccess(): void
    {
        $user = $this->mockAuth();
        $this->addRoleToUser($user->id, 1);
        $activity = Activity::factory(['user_id' => 'abc123'])->create();
        $viewer = Viewer::factory(['user_id' => 'abc123', 'viewer_user_id' => 'bbb222'])->create();
        $response = $this->get('/manage/activity/view/'.$activity->id);
        $response->assertStatus(403);
    }

    protected function addRoleToUser(string $userId, int $roleId): RoleUser
    {
        return RoleUser::create(['user_id' => $userId, 'role_id' => $roleId]);
    }
}
