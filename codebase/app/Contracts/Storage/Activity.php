<?php

namespace App\Contracts\Storage;

use Illuminate\Support\Collection;
use Smorken\Storage\Contracts\Storage\Base;

interface Activity extends Base
{

    public function lastByUserId(string $userId, int $count = 20): Collection;

    public function updateTimeSpent(
        \App\Contracts\Models\Activity $activity,
        int $timeSpent
    ): \App\Contracts\Models\Activity;
}
