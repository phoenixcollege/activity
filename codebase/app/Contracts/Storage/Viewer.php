<?php

namespace App\Contracts\Storage;

use Smorken\Storage\Contracts\Storage\Base;

interface Viewer extends Base
{

    public function removeByViewerUserId(string $viewerId): int;

    public function toggle(string $viewerId, string $userId): bool;
}
