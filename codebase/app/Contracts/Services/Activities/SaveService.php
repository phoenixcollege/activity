<?php

namespace App\Contracts\Services\Activities;

use App\Contracts\Storage\Activity;
use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\HasValidatorService;

interface SaveService extends BaseService, HasValidatorService
{

    public function getMessages(): MessageBag;

    public function getProvider(): Activity;

    public function save(Request $request): SaveResult;

    public function timeSpent(Request $request, int $id): SaveResult;
}
