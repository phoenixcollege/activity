<?php

namespace App\Contracts\Services\Activities;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property \App\Contracts\Models\Activity|null $activity
 * @property \Illuminate\Contracts\Support\MessageBag $messageBag
 */
interface SaveResult extends VOResult
{

}
