<?php

namespace App\Contracts\Services\Activities;

use App\Contracts\Storage\Activity;

interface IndexService extends \Smorken\Service\Contracts\Services\IndexService
{

    public function getProvider(): Activity;
}
