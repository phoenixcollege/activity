<?php

namespace App\Contracts\Services\Viewers\Admin;

use App\Contracts\Storage\Viewer;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;

interface ToggleService extends BaseService
{

    public function getProvider(): Viewer;

    public function toggle(Request $request, string $viewerId): ToggleResult;
}
