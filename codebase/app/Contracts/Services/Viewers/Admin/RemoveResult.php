<?php

namespace App\Contracts\Services\Viewers\Admin;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property string $viewerId
 * @property int $removed
 */
interface RemoveResult extends VOResult
{

}
