<?php

namespace App\Contracts\Services\Viewers\Admin;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property string $viewerId
 * @property string $userId
 * @property bool $status
 */
interface ToggleResult extends VOResult
{

}
