<?php

namespace App\Contracts\Services\Viewers\Admin;

use App\Contracts\Storage\Viewer;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;

interface RemoveService extends BaseService
{

    public function getProvider(): Viewer;

    public function removeAll(Request $request, string $viewerId): RemoveResult;
}
