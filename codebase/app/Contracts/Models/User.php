<?php

namespace App\Contracts\Models;

interface User extends \Smorken\Auth\Proxy\Contracts\Models\User
{

    public function canView(string $userId): bool;
}
