<?php

namespace App\Contracts\Models;

use Smorken\Model\Contracts\HasAttributes;
use Smorken\Model\Contracts\Model;

/**
 * @property int $id
 * @property string $user_id
 * @property string $viewer_user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Contracts\Models\User $user
 * @property \App\Contracts\Models\User $viewer
 */
interface Viewer extends Model, HasAttributes
{

}
