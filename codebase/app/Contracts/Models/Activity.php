<?php

namespace App\Contracts\Models;

use Smorken\Model\Contracts\HasAttributes;
use Smorken\Model\Contracts\Model;

/**
 * @property int $id
 * @property string $user_id
 * @property string $description
 * @property \Carbon\Carbon $started_at
 * @property int $time_spent
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Contracts\Models\User $user
 * @property \Illuminate\Support\Collection<\App\Contracts\Models\Viewer> $viewers
 */
interface Activity extends Model, HasAttributes
{

    public function asFormattedString(): string;

    public function canView(string $viewerId): bool;
}
