<?php

namespace App\Services\Activities;

use App\Contracts\Models\Activity;
use Illuminate\Contracts\Support\MessageBag;
use Smorken\Service\Services\VO\VOResult;

class SaveResult extends VOResult implements \App\Contracts\Services\Activities\SaveResult
{

    public function __construct(
        public ?Activity $activity,
        public MessageBag $messageBag
    ) {
    }
}
