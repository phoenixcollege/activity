<?php

namespace App\Services\Activities\User;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{

    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'f_userId' => $request->user()->id,
            'f_after' => $request->input('f_after'),
            'f_before' => $request->input('f_before'),
        ]);
    }
}
