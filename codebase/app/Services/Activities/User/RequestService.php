<?php

namespace App\Services\Activities\User;

use Illuminate\Http\Request;

class RequestService extends \Smorken\Service\Services\RequestService
{

    protected function modifyRequest(Request $request): Request
    {
        return $request->merge(['user_id' => (string) $request->user()->id]);
    }
}
