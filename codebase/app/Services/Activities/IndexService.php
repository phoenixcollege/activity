<?php

namespace App\Services\Activities;

use App\Contracts\Storage\Activity;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\IndexResult;
use Smorken\Service\Services\BaseService;

class IndexService extends BaseService implements \App\Contracts\Services\Activities\IndexService
{

    protected string $voClass = \Smorken\Service\Services\VO\IndexResult::class;

    public function __construct(protected Activity $activity, array $services = [])
    {
        parent::__construct($services);
    }

    public function getByRequest(Request $request): IndexResult
    {
        $models = $this->getProvider()->lastByUserId($this->getUserId($request));
        return $this->newVO(['models' => $models]);
    }

    protected function getUserId(Request $request): string
    {
        return $request->user()->id;
    }

    public function getProvider(): Activity
    {
        return $this->activity;
    }
}
