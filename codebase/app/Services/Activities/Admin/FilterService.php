<?php

namespace App\Services\Activities\Admin;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{

    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'f_userId' => $request->input('f_userId'),
            'f_after' => $request->input('f_after'),
            'f_before' => $request->input('f_before'),
        ]);
    }
}
