<?php

namespace App\Services\Activities;

use App\Contracts\Services\Activities\SaveResult;
use App\Contracts\Storage\Activity;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Http\Request;
use Smorken\Service\Services\BaseService;
use Smorken\Service\Services\Traits\HasValidatorServiceTrait;

class SaveService extends BaseService implements \App\Contracts\Services\Activities\SaveService
{

    use HasValidatorServiceTrait;

    protected MessageBag $messageBag;

    protected string $voClass = \App\Services\Activities\SaveResult::class;

    public function __construct(protected Activity $activity, array $services = [])
    {
        $this->messageBag = new \Illuminate\Support\MessageBag();
        parent::__construct($services);
    }

    public function getMessages(): MessageBag
    {
        return $this->messageBag;
    }

    public function getProvider(): Activity
    {
        return $this->activity;
    }

    public function save(Request $request): SaveResult
    {
        $data = $this->getData($request);
        $this->validateData($data);
        $activity = $this->getProvider()->create($data);
        if (!$activity) {
            $this->getMessages()->add('description', 'Error saving activity.');
        }
        return $this->newVO(['activity' => $activity, 'messageBag' => $this->getMessages()]);
    }

    public function timeSpent(Request $request, int $id): SaveResult
    {
        $model = $this->getProvider()->findOrFail($id);
        $this->authorizeTimeSpentUpdate($request, $model);
        $timeSpent = $this->getTimeSpentSince($model);
        $this->getProvider()->updateTimeSpent($model, $timeSpent);
        return $this->newVO(['activity' => $model, 'messageBag' => $this->getMessages()]);
    }

    protected function authorizeTimeSpentUpdate(Request $request, \App\Contracts\Models\Activity $activity): bool
    {
        if ($request->user()->id === $activity->user_id) {
            return true;
        }
        throw new AuthorizationException('You are not authorized to update this record.');
    }

    protected function getData(Request $request): array
    {
        return [
            'description' => $request->input('description'),
            'started_at' => $this->getStartedAt($request),
            'time_spent' => $this->getTimeSpent($request),
            'user_id' => $this->getUserId($request),
        ];
    }

    protected function getRules(): array
    {
        return $this->getProvider()->validationRules();
    }

    protected function getStartedAt(Request $request): Carbon
    {
        $raw = $request->input('started_at');
        if ($raw) {
            try {
                return Carbon::parse($raw);
            } catch (\Throwable $e) {
                $this->getMessages()->add('started_at', 'Invalid start time provided. Overridden.');
            }
        }
        return Carbon::now();
    }

    protected function getTimeSpent(Request $request): int
    {
        return (int) $request->input('time_spent');
    }

    protected function getTimeSpentSince(\App\Contracts\Models\Activity $activity): int
    {
        $now = Carbon::now();
        return $now->diffInMinutes($activity->started_at);
    }

    protected function getUserId(Request $request): string
    {
        return $request->user()->id;
    }

    protected function validateData(array $data): bool
    {
        $v = $this->getValidatorService();
        return $v?->validate($data, $this->getRules()) ?? true;
    }
}
