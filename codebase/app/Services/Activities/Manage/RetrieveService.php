<?php

namespace App\Services\Activities\Manage;

use Smorken\Service\Services\RetrieveByStorageProviderService;

class RetrieveService extends RetrieveByStorageProviderService implements
    \App\Contracts\Services\Activities\Manage\RetrieveService
{

    protected function getGateName(string $gateName): string
    {
        return sprintf('Manage.%s', parent::getGateName($gateName));
    }
}
