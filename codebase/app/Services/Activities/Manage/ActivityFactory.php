<?php

namespace App\Services\Activities\Manage;

use Smorken\Service\Services\Factory;

class ActivityFactory extends Factory implements \App\Contracts\Services\Activities\Manage\ActivityFactory
{

    protected array $services = [
        \Smorken\Service\Contracts\Services\FilterService::class => null,
        \App\Contracts\Services\Activities\Manage\ExportService::class => null,
        \App\Contracts\Services\Activities\Manage\IndexService::class => null,
        \App\Contracts\Services\Activities\Manage\RetrieveService::class => null,
    ];
}
