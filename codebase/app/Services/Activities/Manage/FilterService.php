<?php

namespace App\Services\Activities\Manage;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{

    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'f_hasViewer' => $request->user()->id,
            'f_withUser' => true,
            'f_userId' => $request->input('f_userId'),
            'f_after' => $request->input('f_after'),
            'f_before' => $request->input('f_before'),
        ]);
    }
}
