<?php

namespace App\Services\Activities\Manage;

use Smorken\Export\Contracts\Export;
use Smorken\Service\Services\ExportByStorageProviderService;

class ExportService extends ExportByStorageProviderService implements
    \App\Contracts\Services\Activities\Manage\ExportService
{

    protected function getHeaderCallback(...$params): callable|null
    {
        return function (Export $exporter, array $headers, mixed $firstModel): array {
            return ['id', 'user_id', 'user', 'started_at', 'time_spent', 'description', 'created_at'];
        };
    }

    protected function getRowCallback(...$params): callable|null
    {
        return function (Export $exporter, array $data, mixed $model): array {
            return [
                [
                    'id' => $model->id,
                    'user_id' => $model->user_id,
                    'user' => $model->user ? $model->user->name() : 'unknown',
                    'started_at' => $model->started_at,
                    'time_spent' => $model->time_spent,
                    'description' => $model->description,
                    'created_at' => $model->created_at,
                ],
            ];
        };
    }
}
