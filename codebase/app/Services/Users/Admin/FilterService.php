<?php

namespace App\Services\Users\Admin;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{

    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'f_firstName' => $request->input('f_firstName'),
            'f_lastName' => $request->input('f_lastName'),
        ]);
    }
}
