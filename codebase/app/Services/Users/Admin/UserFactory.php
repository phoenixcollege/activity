<?php

namespace App\Services\Users\Admin;

use Smorken\Service\Services\Factory;

class UserFactory extends Factory implements \App\Contracts\Services\Users\Admin\UserFactory
{

    protected array $services = [
        \App\Contracts\Services\Users\Admin\IndexService::class => null,
        \Smorken\Service\Contracts\Services\FilterService::class => null,
    ];
}
