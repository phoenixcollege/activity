<?php

namespace App\Services\Viewers\Admin;

use App\Contracts\Services\Viewers\Admin\ToggleResult;
use App\Contracts\Storage\Viewer;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Smorken\Service\Services\BaseService;

class ToggleService extends BaseService implements \App\Contracts\Services\Viewers\Admin\ToggleService
{

    protected string $voClass = \App\Services\Viewers\Admin\ToggleResult::class;

    public function __construct(protected Viewer $viewer, array $services = [])
    {
        parent::__construct($services);
    }

    public function getProvider(): Viewer
    {
        return $this->viewer;
    }

    public function toggle(Request $request, string $viewerId): ToggleResult
    {
        $userId = $request->input('userId');
        if (!$userId) {
            throw new \OutOfBoundsException('userId field is missing.');
        }
        $status = $this->getProvider()->toggle($viewerId, $userId);
        return $this->newVO(['viewerId' => $viewerId, 'userId' => $userId, 'status' => $status]);
    }

    /**
     * Leave in case I ever decide to allow managers to add users to themselves
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $viewerId
     * @return bool
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function validate(Request $request, string $viewerId): bool
    {
        if ((string) $request->user()->id === $viewerId) {
            return true;
        }
        throw new AuthorizationException('User does not match request.');
    }
}
