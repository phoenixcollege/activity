<?php

namespace App\Services\Viewers\Admin;

use Smorken\Service\Services\VO\VOResult;

class ToggleResult extends VOResult implements \App\Contracts\Services\Viewers\Admin\ToggleResult
{

    public function __construct(
        public string $viewerId,
        public string $userId,
        public bool $status
    ) {
    }
}
