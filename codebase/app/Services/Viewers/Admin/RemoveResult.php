<?php

namespace App\Services\Viewers\Admin;

use Smorken\Service\Services\VO\VOResult;

class RemoveResult extends VOResult implements \App\Contracts\Services\Viewers\Admin\RemoveResult
{

    public function __construct(
        public string $viewerId,
        public int $removed
    ) {
    }
}
