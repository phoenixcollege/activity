<?php

namespace App\Services\Viewers\Admin;

use Smorken\Service\Services\RetrieveByStorageProviderService;

class RetrieveService extends RetrieveByStorageProviderService implements
    \App\Contracts\Services\Viewers\Admin\RetrieveService
{

}
