<?php

namespace App\Services\Viewers\Admin;

use App\Contracts\Services\Viewers\Admin\RemoveResult;
use App\Contracts\Storage\Viewer;
use Illuminate\Http\Request;
use Smorken\Service\Services\BaseService;

class RemoveService extends BaseService implements \App\Contracts\Services\Viewers\Admin\RemoveService
{

    protected string $voClass = \App\Services\Viewers\Admin\RemoveResult::class;

    public function __construct(protected Viewer $viewer, array $services = [])
    {
        parent::__construct($services);
    }

    public function getProvider(): Viewer
    {
        return $this->viewer;
    }

    public function removeAll(Request $request, string $viewerId): RemoveResult
    {
        return $this->newVO([
            'viewerId' => $viewerId, 'removed' => $this->getProvider()->removeByViewerUserId($viewerId),
        ]);
    }
}
