<?php

namespace App\Services\Viewers\Admin;

use Smorken\Service\Services\Factory;

class ViewerFactory extends Factory implements \App\Contracts\Services\Viewers\Admin\ViewerFactory
{

    protected array $services = [
        \App\Contracts\Services\Viewers\Admin\IndexService::class => null,
        \App\Contracts\Services\Viewers\Admin\RemoveService::class => null,
        \App\Contracts\Services\Viewers\Admin\RetrieveService::class => null,
        \App\Contracts\Services\Viewers\Admin\ToggleService::class => null,
        \Smorken\Service\Contracts\Services\FilterService::class => null,
    ];
}
