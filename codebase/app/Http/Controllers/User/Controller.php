<?php

namespace App\Http\Controllers\User;

use App\Contracts\Services\Activities\User\CrudServices;
use App\Contracts\Services\Activities\User\IndexService;
use Smorken\Service\Controllers\CrudController;

class Controller extends CrudController
{

    protected string $baseView = 'user.activity';

    public function __construct(CrudServices $crudServices, IndexService $indexService)
    {
        parent::__construct($crudServices, $indexService);
    }
}
