<?php

namespace App\Http\Controllers\Manage\Activity;

use App\Contracts\Services\Activities\Manage\ActivityFactory;
use App\Contracts\Services\Activities\Manage\ExportService;
use App\Contracts\Services\Activities\Manage\IndexService;
use App\Contracts\Services\Activities\Manage\RetrieveService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Smorken\Service\Contracts\Services\FilterService;

class Controller extends \Smorken\Service\Controllers\Controller
{

    protected string $baseView = 'manage.activity';

    public function __construct(protected ActivityFactory $factory)
    {
        parent::__construct();
    }

    public function export(Request $request): Response
    {
        $result = $this->getExportService()->export($request);
        return $result->exporter->output('activity');
    }

    public function index(Request $request): View
    {
        $result = $this->getIndexService()->getByRequest($request);
        return $this->makeView($this->getViewName('index'))
                    ->with('models', $result->models)
                    ->with('filter', $result->filter);
    }

    public function view(Request $request, int $id): View
    {
        $result = $this->getRetrieveService()->findById($id);
        $filter = $this->getFilterService()->getFilterFromRequest($request);
        return $this->makeView($this->getViewName('view'))
                    ->with('model', $result->model)
                    ->with('filter', $filter);
    }

    protected function getExportService(): ExportService
    {
        return $this->factory->getService(ExportService::class);
    }

    protected function getFilterService(): FilterService
    {
        return $this->factory->getService(FilterService::class);
    }

    protected function getIndexService(): IndexService
    {
        return $this->factory->getService(IndexService::class);
    }

    protected function getRetrieveService(): RetrieveService
    {
        return $this->factory->getService(RetrieveService::class);
    }
}
