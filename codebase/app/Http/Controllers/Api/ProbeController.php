<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Smorken\Service\Controllers\Controller;

class ProbeController extends Controller
{

    public function healthz(Request $request): JsonResponse
    {
        return Response::json(['status' => true], 200);
    }

    public function readyz(Request $request): JsonResponse
    {
        return Response::json(['status' => true], 200);
    }
}
