<?php

namespace App\Http\Controllers\Admin\Activity;

use App\Contracts\Services\Activities\Admin\CrudServices;
use App\Contracts\Services\Activities\Admin\IndexService;
use Smorken\Service\Controllers\CrudController;

class Controller extends CrudController
{

    protected string $baseView = 'admin.activity';

    public function __construct(CrudServices $crudServices, IndexService $indexService)
    {
        parent::__construct($crudServices, $indexService);
    }
}
