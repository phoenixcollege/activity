<?php

namespace App\Http\Controllers\Admin\Viewer;

use App\Contracts\Services\Users\Admin\UserFactory;
use App\Contracts\Services\Viewers\Admin\IndexService;
use App\Contracts\Services\Viewers\Admin\RemoveService;
use App\Contracts\Services\Viewers\Admin\RetrieveService;
use App\Contracts\Services\Viewers\Admin\ToggleService;
use App\Contracts\Services\Viewers\Admin\ViewerFactory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Services\VO\RedirectActionResult;

class Controller extends \Smorken\Service\Controllers\Controller
{

    protected string $baseView = 'admin.viewer';

    public function __construct(
        protected ViewerFactory $services,
        protected UserFactory $userServices
    ) {
        parent::__construct();
    }

    public function index(Request $request): View
    {
        $result = $this->getIndexService()->getByRequest($request);
        return $this->makeView($this->getViewName('index'))
                    ->with('models', $result->models)
                    ->with('filter', $result->filter);
    }

    public function removeAll(Request $request, string $id): RedirectResponse
    {
        $result = $this->getRemoveService()->removeAll($request, $id);
        $request->session()->flash('flash:info', sprintf('Removed %d managed users from %s.', $result->removed, $id));
        $params = $this->getFilterService()->getFilterFromRequest($request)->except(['f_role', 'f_viewers']);
        return (new RedirectActionResult($this->actionArray('index'), $params))->redirect();
    }

    public function toggle(Request $request, string $id): JsonResponse
    {
        $result = $this->getToggleService()->toggle($request, $id);
        return \Illuminate\Support\Facades\Response::json([
            'viewerId' => $result->viewerId,
            'userId' => $result->userId,
            'status' => $result->status,
        ]);
    }

    public function view(Request $request, string $id): View
    {
        $filter = $this->getFilterService()->getFilterFromRequest($request);
        $result = $this->getRetrieveService()->findById($id);
        $usersResult = $this->getUserIndexService()->getByRequest($request);
        return $this->makeView($this->getViewName('view'))
                    ->with('viewer', $result->model)
                    ->with('filter', $filter)
                    ->with('userFilter', $usersResult->filter)
                    ->with('users', $usersResult->models);
    }

    protected function getFilterService(): FilterService
    {
        return $this->services->getService(FilterService::class);
    }

    protected function getIndexService(): IndexService
    {
        return $this->services->getService(IndexService::class);
    }

    protected function getRemoveService(): RemoveService
    {
        return $this->services->getService(RemoveService::class);
    }

    protected function getRetrieveService(): RetrieveService
    {
        return $this->services->getService(RetrieveService::class);
    }

    protected function getToggleService(): ToggleService
    {
        return $this->services->getService(ToggleService::class);
    }

    protected function getUserFilterService(): FilterService
    {
        return $this->userServices->getService(FilterService::class);
    }

    protected function getUserIndexService(): \App\Contracts\Services\Users\Admin\IndexService
    {
        return $this->userServices->getService(\App\Contracts\Services\Users\Admin\IndexService::class);
    }
}
