<?php

namespace App\Http\Controllers;

use App\Contracts\Services\Activities\IndexService;
use App\Contracts\Services\Activities\SaveResult;
use App\Contracts\Services\Activities\SaveService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Smorken\Service\Services\VO\RedirectActionResult;

class HomeController extends \Smorken\Service\Controllers\Controller
{

    public function __construct(
        protected IndexService $indexService,
        protected SaveService $saveService
    ) {
        parent::__construct();
    }

    public function index(Request $request): View
    {
        $result = $this->indexService->getByRequest($request);
        return $this->makeView($this->getViewName('home'))
                    ->with('models', $result->models);
    }

    public function save(Request $request): RedirectResponse|JsonResponse
    {
        $result = $this->saveService->save($request);
        return $this->saveResultToResponse($request, $result);
    }

    public function timeSpent(Request $request, int $id): RedirectResponse|JsonResponse
    {
        $result = $this->saveService->timeSpent($request, $id);
        return $this->saveResultToResponse($request, $result);
    }

    protected function saveResultToResponse(Request $request, SaveResult $result): RedirectResponse|JsonResponse
    {
        if ($request->wantsJson()) {
            return Response::json([
                'id' => $result->activity?->id,
                'last' => $result->activity?->asFormattedString(),
                'errors' => $result->messageBag->toArray(),
            ]);
        }
        return (new RedirectActionResult($this->actionArray('index'), [], $result->messageBag))
            ->redirect();
    }
}
