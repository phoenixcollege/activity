<?php

namespace App\Models\Eloquent;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Smorken\Model\Traits\FriendlyColumns;

class Activity extends Base implements \App\Contracts\Models\Activity
{

    use FriendlyColumns, HasFactory;

    protected $fillable = ['user_id', 'description', 'started_at', 'time_spent'];

    protected array $friendly_columns = [
        'id' => 'ID',
        'user_id' => 'User ID',
        'description' => 'Description',
        'started_at' => 'Started At',
        'time_spent' => 'Time Spent',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
    ];

    protected array $rules = [
        'user_id' => 'required|string|max:32',
        'description' => 'required|string|max:255',
        'started_at' => 'date',
        'time_spent' => 'int|max:999',
    ];

    public function asFormattedString(): string
    {
        $formatter = '%s%s'.PHP_EOL.'%s';
        return sprintf($formatter, $this->started_at, $this->getFormattedTimeSpent(), $this->description);
    }

    public function canView(string $viewerId): bool
    {
        foreach ($this->viewers as $viewer) {
            if ($viewer->viewer_user_id === $viewerId) {
                return true;
            }
        }
        return false;
    }

    public function getStartedAtAttribute(): ?Carbon
    {
        return $this->toCarbonDate('started_at');
    }

    public function scopeBetweenDates(Builder $query, Carbon $start, Carbon $end): Builder
    {
        return $query->whereDate('started_at', '>=', $start)
                     ->whereDate('started_at', '<=', $end);
    }

    public function scopeDateAfter(Builder $query, Carbon $date): Builder
    {
        return $query->whereDate('started_at', '>=', $date);
    }

    public function scopeDateBefore(Builder $query, Carbon $date): Builder
    {
        return $query->whereDate('started_at', '<=', $date);
    }

    public function scopeDateIs(Builder $query, Carbon $date): Builder
    {
        return $query->whereDate('started_at', '=', $date);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('started_at', 'desc');
    }

    public function scopeHasViewer(Builder $query, string $viewerId): Builder
    {
        return $query->whereHas('viewers', function ($q) use ($viewerId) {
            $q->where('viewer_user_id', '=', $viewerId);
        });
    }

    public function scopeUserIdIs(Builder $query, string $id): Builder
    {
        return $query->where('user_id', '=', $id);
    }

    public function scopeWithUser(Builder $query): Builder
    {
        return $query->with('user');
    }

    public function setStartedAtAttribute($value): void
    {
        $this->attributes['started_at'] = Carbon::parse($value);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function viewers(): HasMany
    {
        return $this->hasMany(Viewer::class, 'user_id', 'user_id');
    }

    protected function getFormattedTimeSpent(): string
    {
        if ($this->time_spent > 0) {
            return sprintf(' for %d minutes', $this->time_spent);
        }
        return '';
    }
}
