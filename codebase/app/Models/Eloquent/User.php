<?php

namespace App\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends \Smorken\Auth\Proxy\Models\Eloquent\User implements \App\Contracts\Models\User
{

    public function canView(string $userId): bool
    {
        foreach ($this->viewers as $viewer) {
            if ($viewer->user_id === $userId) {
                return true;
            }
        }
        return false;
    }

    public function scopeWithViewers(Builder $query): Builder
    {
        return $query->with('viewers');
    }

    public function viewers(): HasMany
    {
        return $this->hasMany(Viewer::class, 'viewer_user_id');
    }
}
