<?php

namespace App\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Viewer extends Base implements \App\Contracts\Models\Viewer
{

    use HasFactory;

    protected $fillable = ['user_id', 'viewer_user_id'];

    protected array $rules = [
        'user_id' => 'required|string|max:32',
        'viewer_user_id' => 'required|string|max:32',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function viewer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'viewer_user_id');
    }

    public function scopeWithUser(Builder $query): Builder
    {
        return $query->with('user');
    }

    public function scopeWithViewer(Builder $query): Builder
    {
        return $query->with('viewer');
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('viewer_user_id')
                     ->orderBy('user_id');
    }

    public function scopeUserIdIs(Builder $query, string $id): Builder
    {
        return $query->where('user_id', '=', $id);
    }

    public function scopeViewerUserIdIs(Builder $query, string $id): Builder
    {
        return $query->where('viewer_user_id', '=', $id);
    }
}
