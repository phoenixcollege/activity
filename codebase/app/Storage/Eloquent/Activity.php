<?php

namespace App\Storage\Eloquent;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class Activity extends Base implements \App\Contracts\Storage\Activity
{

    public function lastByUserId(string $userId, int $count = 20): Collection
    {
        return $this->getModel()
                    ->newQuery()
                    ->userIdIs($userId)
                    ->dateIs(Carbon::now())
                    ->defaultOrder()
                    ->limit($count)
                    ->get();
    }

    public function updateTimeSpent(
        \App\Contracts\Models\Activity $activity,
        int $timeSpent
    ): \App\Contracts\Models\Activity {
        $activity->time_spent = $timeSpent;
        $activity->save();
        return $activity;
    }

    protected function filterHasViewer(Builder $query, $value): Builder
    {
        if (strlen($value)) {
            $query->hasViewer($value);
        }
        return $query;
    }

    protected function filterStartedAfter(Builder $query, $value): Builder
    {
        $value = $this->toCarbon($value);
        if ($value) {
            $query->dateAfter($value);
        }
        return $query;
    }

    protected function filterStartedBefore(Builder $query, $value): Builder
    {
        $value = $this->toCarbon($value);
        if ($value) {
            $query->dateBefore($value);
        }
        return $query;
    }

    protected function filterUserId(Builder $query, $value): Builder
    {
        if (strlen($value)) {
            $query->userIdIs($value);
        }
        return $query;
    }

    protected function filterWithUser(Builder $query, $value): Builder
    {
        if ($value === true) {
            $query->withUser();
        }
        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_userId' => 'filterUserId',
            'f_after' => 'filterStartedAfter',
            'f_before' => 'filterStartedBefore',
            'f_hasViewer' => 'filterHasViewer',
            'f_withUser' => 'filterWithUser',
        ];
    }

    protected function toCarbon(?string $date): ?Carbon
    {
        if ($date && strlen($date)) {
            try {
                return Carbon::parse($date);
            } catch (\Throwable $e) {
                // pass
            }
        }
        return null;
    }
}
