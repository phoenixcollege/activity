<?php

namespace App\Storage\Eloquent;

class Viewer extends Base implements \App\Contracts\Storage\Viewer
{

    public function removeByViewerUserId(string $viewerId): int
    {
        return $this->getModel()->newQuery()->viewerUserIdIs($viewerId)->delete();
    }

    public function toggle(string $viewerId, string $userId): bool
    {
        $baseQuery = $this->getModel()->newQuery()->viewerUserIdIs($viewerId)->userIdIs($userId);
        if ($baseQuery->exists()) {
            return ($baseQuery->delete() === 0);
        }
        return (bool) $this->create(['viewer_user_id' => $viewerId, 'user_id' => $userId]);
    }
}
