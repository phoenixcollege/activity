<?php

namespace App\Storage\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;

class User extends Base implements \App\Contracts\Storage\User
{

    protected function filterFirstName(Builder $q, $v): Builder
    {
        if (strlen($v)) {
            $q->firstNameLike($v);
        }
        return $q;
    }

    protected function filterLastName(Builder $q, $v): Builder
    {
        if (strlen($v)) {
            $q->lastNameLike($v);
        }
        return $q;
    }

    protected function filterRoleByCode(Builder $q, $v): Builder
    {
        if (strlen($v)) {
            $q->roleIncludesCode($v);
        }
        return $q;
    }

    protected function filterWithViewers(Builder $q, $v): Builder
    {
        if ($v === true) {
            $q->withViewers($v);
        }
        return $q;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_lastName' => 'filterLastName',
            'f_firstName' => 'filterFirstName',
            'f_ulastName' => 'filterLastName',
            'f_ufirstName' => 'filterFirstName',
            'f_role' => 'filterRoleByCode',
            'f_viewers' => 'filterWithViewers',
        ];
    }
}
