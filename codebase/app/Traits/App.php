<?php

namespace App\Traits;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;

trait App
{

    /**
     * @var Container|Application|null
     */
    protected Application|Container|null $app = null;

    /**
     * @var array<string, mixed>
     */
    protected array $providers = [];

    /**
     * @return Container|Application
     */
    public function getApp(): Container|Application
    {
        if (is_null($this->app)) {
            $this->app = \Illuminate\Support\Facades\App::getFacadeRoot();
        }
        return $this->app;
    }

    /**
     * @param  Application  $app
     */
    public function setApp(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param  string  $contract
     * @param  array  $params
     * @return mixed
     */
    protected function make(string $contract, array $params = [])
    {
        if (!isset($this->providers[$contract])) {
            try {
                $this->providers[$contract] = $this->getApp()->make($contract, $params);
            } catch (BindingResolutionException $e) {
                $trimmed = trim($contract, "\\");
                $this->providers[$contract] = $this->getApp()->make($trimmed, $params);
            }
        }
        return $this->providers[$contract];
    }
}
