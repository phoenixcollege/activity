<?php

namespace App\Providers\Services\User;

use App\Contracts\Services\Activities\User\CrudServices;
use App\Contracts\Services\Activities\User\IndexService;
use App\Contracts\Storage\Activity;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\GateService;
use Smorken\Service\Contracts\Services\RequestService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;

class ActivityServices extends Invokable
{

    public static function defineGates(Gate $gate): void
    {
        $func = function (Authenticatable $user, \App\Contracts\Models\Activity $activity) {
            $authId = (string) $user->getAuthIdentifier();
            return $authId && $authId === $activity->user_id;
        };
        $gate->define('Activity.delete', $func);
        $gate->define('Activity.view', $func);
        $gate->define('Activity.update', $func);
    }

    public function __invoke()
    {
        $this->getApp()->bind(IndexService::class, function ($app) {
            return new \App\Services\Activities\User\IndexService(
                $app[Activity::class],
                [
                    FilterService::class => new \App\Services\Activities\User\FilterService(),
                ]
            );
        });
        $this->getApp()->bind(CrudServices::class, function ($app) {
            $provider = $app[Activity::class];
            $gate = $app[Gate::class];
            self::defineGates($gate);
            $services = [
                FilterService::class => new \App\Services\Activities\User\FilterService(),
                GateService::class => new \Smorken\Service\Services\GateService($gate),
                RequestService::class => new \App\Services\Activities\User\RequestService(),
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ];
            return \App\Services\Activities\User\CrudServices::createByStorageProvider($provider, $services);
        });
    }
}
