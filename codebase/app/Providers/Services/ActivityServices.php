<?php

namespace App\Providers\Services;

use App\Contracts\Services\Activities\IndexService;
use App\Contracts\Services\Activities\SaveService;
use App\Contracts\Storage\Activity;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;

class ActivityServices extends Invokable
{

    public function __invoke()
    {
        $this->getApp()->bind(IndexService::class, function ($app) {
            return new \App\Services\Activities\IndexService($app[Activity::class]);
        });
        $this->getApp()->bind(SaveService::class, function ($app) {
            return new \App\Services\Activities\SaveService($app[Activity::class], [
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ]);
        });
    }
}
