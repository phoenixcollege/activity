<?php

namespace App\Providers\Services\Manage;

use App\Contracts\Services\Activities\Manage\ActivityFactory;
use App\Contracts\Services\Activities\Manage\ExportService;
use App\Contracts\Services\Activities\Manage\IndexService;
use App\Contracts\Services\Activities\Manage\RetrieveService;
use App\Contracts\Storage\Activity;
use App\Services\Activities\Manage\FilterService;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Export\Contracts\Export;
use Smorken\Service\Contracts\Services\GateService;
use Smorken\Service\Invokables\Invokable;

class ActivityServices extends Invokable
{

    public static function defineGates(Gate $gate): void
    {
        $func = function (Authenticatable $user, \App\Contracts\Models\Activity $activity) {
            $authId = (string) $user->getAuthIdentifier();
            return $authId && $activity->canView($authId);
        };
        $gate->define('Manage.Activity.view', $func);
    }

    public function __invoke()
    {
        $this->getApp()->bind(ActivityFactory::class, function ($app) {
            $provider = $app[Activity::class];
            $filterService = new FilterService();
            $gate = $app[Gate::class];
            self::defineGates($gate);
            $services = [
                ExportService::class => new \App\Services\Activities\Manage\ExportService(
                    $app[Export::class],
                    $provider
                ),
                IndexService::class => new \App\Services\Activities\Manage\IndexService(
                    $provider
                ),
                RetrieveService::class => new \App\Services\Activities\Manage\RetrieveService(
                    $provider
                ),
                \Smorken\Service\Contracts\Services\FilterService::class => $filterService,
            ];
            $additionalServices = [
                \Smorken\Service\Contracts\Services\FilterService::class => $filterService,
                GateService::class => new \Smorken\Service\Services\GateService($gate),
            ];
            return new \App\Services\Activities\Manage\ActivityFactory($services, $additionalServices);
        });
    }
}
