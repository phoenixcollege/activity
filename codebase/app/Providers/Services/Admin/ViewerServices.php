<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Viewers\Admin\IndexService;
use App\Contracts\Services\Viewers\Admin\RemoveService;
use App\Contracts\Services\Viewers\Admin\RetrieveService;
use App\Contracts\Services\Viewers\Admin\ToggleService;
use App\Contracts\Storage\User;
use App\Contracts\Storage\Viewer;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Invokables\Invokable;

class ViewerServices extends Invokable
{

    public function __invoke()
    {
        $this->getApp()->bind(IndexService::class, function ($app) {
            return new \App\Services\Viewers\Admin\IndexService(
                $app[User::class],
                [
                    FilterService::class => new \App\Services\Viewers\Admin\FilterService(),
                ]
            );
        });
        $this->getApp()->bind(\App\Contracts\Services\Viewers\Admin\ViewerFactory::class, function ($app) {
            $userProvider = $app[User::class];
            $viewerProvider = $app[Viewer::class];
            $filterService = new \App\Services\Viewers\Admin\FilterService();
            $services = [
                IndexService::class => new \App\Services\Viewers\Admin\IndexService(
                    $userProvider
                ),
                FilterService::class => new \App\Services\Viewers\Admin\FilterService(),
                RemoveService::class => new \App\Services\Viewers\Admin\RemoveService($viewerProvider),
                RetrieveService::class => new \App\Services\Viewers\Admin\RetrieveService($userProvider),
                ToggleService::class => new \App\Services\Viewers\Admin\ToggleService($viewerProvider),
            ];
            $additionalServices = [
                FilterService::class => $filterService,
            ];
            return new \App\Services\Viewers\Admin\ViewerFactory($services, $additionalServices);
        });
    }
}
