<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Users\Admin\IndexService;
use App\Contracts\Services\Users\Admin\UserFactory;
use App\Contracts\Storage\User;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Invokables\Invokable;

class UserServices extends Invokable
{

    public function __invoke()
    {
        $this->getApp()->bind(UserFactory::class, function ($app) {
            $provider = $app[User::class];
            $services = [
                IndexService::class => new \App\Services\Users\Admin\IndexService($provider),
            ];
            $additionalServices = [
                FilterService::class => new \App\Services\Users\Admin\FilterService(),
            ];
            return new \App\Services\Users\Admin\UserFactory($services, $additionalServices);
        });
    }
}
