<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Activities\Admin\CrudServices;
use App\Contracts\Services\Activities\Admin\IndexService;
use App\Contracts\Storage\Activity;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;

class ActivityServices extends Invokable
{

    public function __invoke()
    {
        $this->getApp()->bind(IndexService::class, function ($app) {
            return new \App\Services\Activities\Admin\IndexService(
                $app[Activity::class],
                [
                    FilterService::class => new \App\Services\Activities\Admin\FilterService(),
                ]
            );
        });
        $this->getApp()->bind(CrudServices::class, function ($app) {
            $provider = $app[Activity::class];
            $services = [
                FilterService::class => new \App\Services\Activities\Admin\FilterService(),
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ];
            return \App\Services\Activities\Admin\CrudServices::createByStorageProvider($provider, $services);
        });
    }
}
