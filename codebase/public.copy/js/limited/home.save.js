/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/limited/_modules/ajax.fail.js":
/*!****************************************************!*\
  !*** ./resources/js/limited/_modules/ajax.fail.js ***!
  \****************************************************/
/***/ ((module) => {

var ajaxFail = {
  fail: function fail(data) {
    var response = data.response || {};

    if (response.status === 422) {
      return;
    }

    if (response.status > 299 && response.status < 500 && response.status !== 422) {
      window.location.reload();
    } else {
      var message = 'There was an error connecting to the backend.';
      alert(message);
      console.log(data);
    }
  }
};
module.exports = ajaxFail;

/***/ }),

/***/ "./resources/js/limited/_modules/axios.base.js":
/*!*****************************************************!*\
  !*** ./resources/js/limited/_modules/axios.base.js ***!
  \*****************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ajaxFail = __webpack_require__(/*! ./ajax.fail */ "./resources/js/limited/_modules/ajax.fail.js");

var Loading = __webpack_require__(/*! ./loading */ "./resources/js/limited/_modules/loading.js");

var AxiosBase = /*#__PURE__*/function () {
  function AxiosBase() {
    _classCallCheck(this, AxiosBase);

    this.loading = new Loading('ajax-loading');
  }

  _createClass(AxiosBase, [{
    key: "doGet",
    value: function doGet(url, success, failure, _final) {
      var _this = this;

      this.loading.start();
      axios.get(url).then(function (response) {
        success(response);
      })["catch"](function (error) {
        failure(error);
        ajaxFail.fail(error);
      })["finally"](function () {
        _this.loading.stop();

        if (typeof _final === 'function') {
          _final();
        }
      });
    }
  }, {
    key: "doPost",
    value: function doPost(url, data, success, failure, _final2) {
      var _this2 = this;

      this.loading.start();
      axios.post(url, data).then(function (response) {
        success(response);
      })["catch"](function (error) {
        failure(error);
        ajaxFail.fail(error);
      })["finally"](function () {
        _this2.loading.stop();

        if (typeof _final2 === 'function') {
          _final2();
        }
      });
    }
  }, {
    key: "sleep",
    value: function sleep(milliseconds) {
      var date = Date.now();
      var currentDate = null;

      do {
        currentDate = Date.now();
      } while (currentDate - date < milliseconds);
    }
  }, {
    key: "getUrl",
    value: function getUrl(id) {
      return document.getElementById(id).getAttribute('href');
    }
  }]);

  return AxiosBase;
}();

module.exports = AxiosBase;

/***/ }),

/***/ "./resources/js/limited/_modules/home.save.axios.js":
/*!**********************************************************!*\
  !*** ./resources/js/limited/_modules/home.save.axios.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var AxiosBase = __webpack_require__(/*! ./axios.base */ "./resources/js/limited/_modules/axios.base.js");

var HomeSaveAxios = /*#__PURE__*/function (_AxiosBase) {
  _inherits(HomeSaveAxios, _AxiosBase);

  var _super = _createSuper(HomeSaveAxios);

  function HomeSaveAxios() {
    _classCallCheck(this, HomeSaveAxios);

    return _super.apply(this, arguments);
  }

  _createClass(HomeSaveAxios, [{
    key: "save",
    value: function save(url, data, success, failure) {
      this.doPost(url, data, success, failure);
    }
  }]);

  return HomeSaveAxios;
}(AxiosBase);

module.exports = HomeSaveAxios;

/***/ }),

/***/ "./resources/js/limited/_modules/home.timespent.axios.js":
/*!***************************************************************!*\
  !*** ./resources/js/limited/_modules/home.timespent.axios.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var AxiosBase = __webpack_require__(/*! ./axios.base */ "./resources/js/limited/_modules/axios.base.js");

var HomeTimespentAxios = /*#__PURE__*/function (_AxiosBase) {
  _inherits(HomeTimespentAxios, _AxiosBase);

  var _super = _createSuper(HomeTimespentAxios);

  function HomeTimespentAxios() {
    _classCallCheck(this, HomeTimespentAxios);

    return _super.apply(this, arguments);
  }

  _createClass(HomeTimespentAxios, [{
    key: "timespent",
    value: function timespent(url, id, success, failure) {
      url = url.replace('ID', id);
      this.doPost(url, {}, success, failure);
    }
  }]);

  return HomeTimespentAxios;
}(AxiosBase);

module.exports = HomeTimespentAxios;

/***/ }),

/***/ "./resources/js/limited/_modules/loading.js":
/*!**************************************************!*\
  !*** ./resources/js/limited/_modules/loading.js ***!
  \**************************************************/
/***/ ((module) => {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Loading = /*#__PURE__*/function () {
  function Loading(loading_id) {
    var divX = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
    var divY = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 4;

    _classCallCheck(this, Loading);

    this.loading_id = loading_id;
    this.getElement().style.zIndex = '999';
  }

  _createClass(Loading, [{
    key: "start",
    value: function start() {
      var el = this.getElement();
      el.style.display = 'block';
    }
  }, {
    key: "stop",
    value: function stop() {
      this.getElement().style.display = 'none';
    }
  }, {
    key: "getElement",
    value: function getElement() {
      return document.getElementById(this.loading_id);
    }
  }]);

  return Loading;
}();

module.exports = Loading;

/***/ }),

/***/ "./resources/js/limited/_modules/template/activity.template.js":
/*!*********************************************************************!*\
  !*** ./resources/js/limited/_modules/template/activity.template.js ***!
  \*********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var BaseTemplate = __webpack_require__(/*! ./base.template */ "./resources/js/limited/_modules/template/base.template.js");

var ActivityTemplate = /*#__PURE__*/function (_BaseTemplate) {
  _inherits(ActivityTemplate, _BaseTemplate);

  var _super = _createSuper(ActivityTemplate);

  function ActivityTemplate() {
    _classCallCheck(this, ActivityTemplate);

    return _super.apply(this, arguments);
  }

  _createClass(ActivityTemplate, [{
    key: "render",
    value: function render(data, isNew) {
      var ele = this.getElement(data, isNew);
      this.handleElementData(data, ele, isNew);

      if (isNew === true) {
        this.prependToContainer(ele, this.getContainer());
      }
    }
  }, {
    key: "getElement",
    value: function getElement(data, isNew) {
      var ele = null;

      if (isNew === false) {
        ele = this.getExistingElement(data);
      }

      if (!ele) {
        ele = this.createTemplatedElement(data);
      }

      return ele;
    }
  }, {
    key: "getExistingElement",
    value: function getExistingElement(data) {
      var elements = document.getElementsByClassName('activity');
      return this.getElementById(data.id || 0, elements);
    }
  }, {
    key: "getElementById",
    value: function getElementById(id, elements) {
      id = parseInt(id);

      var _iterator = _createForOfIteratorHelper(elements),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var element = _step.value;
          var elementId = parseInt(element.dataset.id);

          if (id > 0 && id === elementId) {
            return element;
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
    }
  }, {
    key: "handleElementData",
    value: function handleElementData(data, element, isNew) {
      this.addTextToPre(data.last || '', element);

      if (isNew === true) {
        element.dataset.id = data.id;
        element.id = 'activity-' + data.id;
      }
    }
  }, {
    key: "addTextToPre",
    value: function addTextToPre(text, element) {
      var pre = element.getElementsByTagName('pre')[0];

      if (pre) {
        pre.innerText = text;
      }
    }
  }, {
    key: "createTemplatedElement",
    value: function createTemplatedElement(data) {
      return this.cloneTemplate(this.getTemplate());
    }
  }]);

  return ActivityTemplate;
}(BaseTemplate);

module.exports = ActivityTemplate;

/***/ }),

/***/ "./resources/js/limited/_modules/template/base.template.js":
/*!*****************************************************************!*\
  !*** ./resources/js/limited/_modules/template/base.template.js ***!
  \*****************************************************************/
/***/ ((module) => {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var BaseTemplate = /*#__PURE__*/function () {
  function BaseTemplate(template, container) {
    _classCallCheck(this, BaseTemplate);

    this.template = template;
    this.container = container;
  }

  _createClass(BaseTemplate, [{
    key: "render",
    value: function render(data) {
      var ele = this.createTemplatedElement(data);
      this.appendToContainer(ele, this.getContainer());
    }
  }, {
    key: "createTemplatedElement",
    value: function createTemplatedElement(innerText) {
      var ele = this.cloneTemplate(this.getTemplate());
      ele.innerText = innerText;
      return ele;
    }
  }, {
    key: "getTemplate",
    value: function getTemplate() {
      return this.template;
    }
  }, {
    key: "getContainer",
    value: function getContainer() {
      return this.container;
    }
  }, {
    key: "appendToContainer",
    value: function appendToContainer(element, container) {
      container.appendChild(element);
    }
  }, {
    key: "prependToContainer",
    value: function prependToContainer(element, container) {
      container.prepend(element);
    }
  }, {
    key: "cloneTemplate",
    value: function cloneTemplate(template) {
      return template.content.firstElementChild.cloneNode(true);
    }
  }]);

  return BaseTemplate;
}();

module.exports = BaseTemplate;

/***/ }),

/***/ "./resources/js/limited/_modules/template/errors.template.js":
/*!*******************************************************************!*\
  !*** ./resources/js/limited/_modules/template/errors.template.js ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var BaseTemplate = __webpack_require__(/*! ./base.template */ "./resources/js/limited/_modules/template/base.template.js");

var ErrorsTemplate = /*#__PURE__*/function (_BaseTemplate) {
  _inherits(ErrorsTemplate, _BaseTemplate);

  var _super = _createSuper(ErrorsTemplate);

  function ErrorsTemplate() {
    _classCallCheck(this, ErrorsTemplate);

    return _super.apply(this, arguments);
  }

  _createClass(ErrorsTemplate, [{
    key: "clear",
    value: function clear() {
      this.getContainer().textContent = '';
    }
  }, {
    key: "autoClear",
    value: function autoClear() {
      var _this = this;

      setTimeout(function () {
        return _this.clear();
      }, 5000);
    }
  }, {
    key: "render",
    value: function render(data) {
      this.clear();
      var shouldAutoClear = false;

      for (var _i = 0, _Object$entries = Object.entries(data); _i < _Object$entries.length; _i++) {
        var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
            key = _Object$entries$_i[0],
            moreErrors = _Object$entries$_i[1];

        var _iterator = _createForOfIteratorHelper(moreErrors),
            _step;

        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var error = _step.value;
            shouldAutoClear = true;
            this.renderError(error);
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }

      if (shouldAutoClear) {
        this.autoClear();
      }
    }
  }, {
    key: "renderError",
    value: function renderError(error) {
      var tpl = this.createTemplatedElement(error);
      this.appendToContainer(tpl, this.getContainer());
    }
  }]);

  return ErrorsTemplate;
}(BaseTemplate);

module.exports = ErrorsTemplate;

/***/ }),

/***/ "./resources/js/limited/_modules/templates.js":
/*!****************************************************!*\
  !*** ./resources/js/limited/_modules/templates.js ***!
  \****************************************************/
/***/ ((module) => {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Templates = /*#__PURE__*/function () {
  function Templates(templates) {
    _classCallCheck(this, Templates);

    this.templates = templates;
  }

  _createClass(Templates, [{
    key: "get",
    value: function get(key) {
      var _ref = this.templates[key] || {},
          classBuilder = _ref.classBuilder,
          templateId = _ref.templateId,
          containerId = _ref.containerId;

      if (classBuilder && templateId && containerId) {
        return new classBuilder(document.getElementById(templateId), document.getElementById(containerId));
      }
    }
  }]);

  return Templates;
}();

module.exports = Templates;

/***/ }),

/***/ "./resources/js/limited/_modules/urls.js":
/*!***********************************************!*\
  !*** ./resources/js/limited/_modules/urls.js ***!
  \***********************************************/
/***/ ((module) => {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Urls = /*#__PURE__*/function () {
  function Urls(urls) {
    _classCallCheck(this, Urls);

    this.urls = urls;
  }

  _createClass(Urls, [{
    key: "get",
    value: function get(key) {
      var element = document.getElementById(this.urls[key]);

      if (element) {
        return element.getAttribute('href');
      }
    }
  }]);

  return Urls;
}();

module.exports = Urls;

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!*******************************************!*\
  !*** ./resources/js/limited/home.save.js ***!
  \*******************************************/
var HomeSaveAxios = __webpack_require__(/*! ./_modules/home.save.axios */ "./resources/js/limited/_modules/home.save.axios.js");

var HomeTimespentAxios = __webpack_require__(/*! ./_modules/home.timespent.axios */ "./resources/js/limited/_modules/home.timespent.axios.js");

var ErrorsTemplate = __webpack_require__(/*! ./_modules/template/errors.template */ "./resources/js/limited/_modules/template/errors.template.js");

var ActivityTemplate = __webpack_require__(/*! ./_modules/template/activity.template */ "./resources/js/limited/_modules/template/activity.template.js");

var Urls = __webpack_require__(/*! ./_modules/urls */ "./resources/js/limited/_modules/urls.js");

var Templates = __webpack_require__(/*! ./_modules/templates */ "./resources/js/limited/_modules/templates.js");

var templates = new Templates({
  errors: {
    classBuilder: ErrorsTemplate,
    templateId: 'error-tpl',
    containerId: 'errors'
  },
  activities: {
    classBuilder: ActivityTemplate,
    templateId: 'activity-tpl',
    containerId: 'activities'
  }
});
var urls = new Urls({
  saveUrlId: 'save-url',
  timeSpentUrlId: 'time-spent-url'
});
var Activities = {
  formId: 'save-form',
  init: function init() {
    Activities.Form.init();
    Activities.TimeSpent.init();
  },
  handleErrors: function handleErrors(data) {
    templates.get('errors').render(data.response.data.errors || {});
  },
  Form: {
    init: function init() {
      var form = Activities.Form.getForm();
      form.addEventListener('submit', function (event) {
        Activities.Form.handle(event, form);
      });
    },
    getForm: function getForm() {
      return document.getElementById(Activities.formId);
    },
    handle: function handle(event, form) {
      event.preventDefault();
      var formData = new FormData(form);
      var url = urls.get('saveUrlId');

      if (url) {
        var saver = new HomeSaveAxios();
        saver.save(url, formData, Activities.Form.success, Activities.handleErrors);
      }
    },
    success: function success(data) {
      templates.get('activities').render(data.data || {}, true);
      Activities.Form.getForm().reset();
    }
  },
  TimeSpent: {
    init: function init() {
      document.addEventListener('click', function (event) {
        Activities.TimeSpent.handle(event);
      });
    },
    getId: function getId(element) {
      var container = element.parentNode.parentNode;
      return container.dataset.id;
    },
    handle: function handle(event) {
      if (!event.target.matches('.time-spent')) {
        return;
      }

      event.preventDefault();
      var url = urls.get('timeSpentUrlId');
      var id = Activities.TimeSpent.getId(event.target);

      if (url && id) {
        var ts = new HomeTimespentAxios();
        ts.timespent(url, id, Activities.TimeSpent.success, Activities.handleErrors);
      }
    },
    success: function success(data) {
      templates.get('activities').render(data.data || {}, false);
    }
  }
};
document.addEventListener('DOMContentLoaded', function () {
  Activities.init();
});
})();

/******/ })()
;