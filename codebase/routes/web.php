<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])
     ->group(function () {
         Route::get('/', [\App\Http\Controllers\HomeController::class, 'index']);
         Route::post('/', [\App\Http\Controllers\HomeController::class, 'save']);
         Route::post('/time-spent/{id}', [\App\Http\Controllers\HomeController::class, 'timeSpent']);

         Route::group([
             'prefix' => 'user',
         ], function () {
             \Smorken\Support\Routes::create(\App\Http\Controllers\User\Controller::class);
         });
     });

Route::group([
    'prefix' => 'manage',
    'middleware' => ['auth', 'can:role-manage'],
], function () {
    Route::group([
        'prefix' => 'activity',
    ], function () {
        $controller = \App\Http\Controllers\Manage\Activity\Controller::class;
        Route::get('/', [$controller, 'index']);
        Route::get('/view/{id}', [$controller, 'view']);
        Route::get('/export', [$controller, 'export']);
    });
});

Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth', 'can:role-admin'],
], function () {
    Route::group([
        'prefix' => 'activity',
    ], function () {
        \Smorken\Support\Routes::create(\App\Http\Controllers\Admin\Activity\Controller::class);
    });

    Route::group([
        'prefix' => 'managers',
    ], function () {
        $controller = \App\Http\Controllers\Admin\Viewer\Controller::class;
        Route::get('/', [$controller, 'index']);
        Route::get('/view/{id}', [$controller, 'view']);
        Route::get('/remove-all/{id}', [$controller, 'removeAll']);
        Route::post('/toggle/{id}', [$controller, 'toggle']);
    });
});
