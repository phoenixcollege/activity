<?php

use Smorken\Auth\Proxy\Http\Controllers\Admin\User\Controller;

return [
    'guest' => [],
    'auth' => [
        [
            'name' => 'My Activity',
            'action' => [\App\Http\Controllers\User\Controller::class, 'index'],
            'children' => [],
        ],
    ],

    'role-manage' => [
        [
            'name' => 'Activities',
            'action' => [\App\Http\Controllers\Manage\Activity\Controller::class, 'index'],
            'children' => [],
        ],
    ],
    'role-admin' => [
        [
            'name' => 'Activities',
            'action' => [\App\Http\Controllers\Admin\Activity\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Managers',
            'action' => [\App\Http\Controllers\Admin\Viewer\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Users',
            'action' => [Controller::class, 'index'],
            'children' => [],
        ],
    ],
];
