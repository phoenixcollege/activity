<?php
return [
    'concrete' => [
        \App\Storage\Eloquent\Activity::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Activity::class,
            ],
        ],
        \App\Storage\Eloquent\User::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\User::class,
            ],
        ],
        \App\Storage\Eloquent\Viewer::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Viewer::class,
            ],
        ],
    ],
    'contract' => [
        \App\Contracts\Storage\Activity::class => \App\Storage\Eloquent\Activity::class,
        \App\Contracts\Storage\User::class => \App\Storage\Eloquent\User::class,
        \App\Contracts\Storage\Viewer::class => \App\Storage\Eloquent\Viewer::class,
    ],
];
