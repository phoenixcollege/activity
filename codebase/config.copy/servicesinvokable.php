<?php
return [
    'autoload' => env('SERVICES_AUTOLOAD', false),
    'path' => env('SERVICES_INVOKABLE_PATH', 'Providers/Services'),
    'invokables' => [
        \App\Providers\Services\ActivityServices::class,
        \App\Providers\Services\Admin\ActivityServices::class,
        \App\Providers\Services\Admin\UserServices::class,
        \App\Providers\Services\Admin\ViewerServices::class,
        \App\Providers\Services\Manage\ActivityServices::class,
        \App\Providers\Services\User\ActivityServices::class,
    ],
];
