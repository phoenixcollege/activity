<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Smorken\Model\Contracts\Model;

abstract class StorageSeeder extends Seeder
{

    protected mixed $provider;

    protected ?string $providerClass = null;

    abstract protected function getData(): array;

    public function run(): void
    {
        $this->truncate();
        foreach ($this->getData() as $row) {
            $this->create($row);
        }
    }

    protected function create(array $attributes): Model
    {
        return $this->getModel()
                    ->create($attributes);
    }

    protected function getModel(): Model
    {
        return $this->getProvider()->getModel();
    }

    protected function getProvider(): mixed
    {
        if (is_null($this->provider) && $this->providerClass) {
            $this->provider = $this->container->make($this->providerClass);
        }
        return $this->provider;
    }

    protected function truncate(): mixed
    {
        return $this->getModel()
                    ->truncate();
    }
}
