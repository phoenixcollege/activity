<?php

namespace Database\Seeders;

use Database\Seeders\Smorken\Roles\Models\Eloquent\RoleSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run(): void
    {
        $this->call($this->getSeeders());
    }

    protected function getSeeders(): array
    {
        $seeders = [];
        if (class_exists(RoleSeeder::class)) {
            $seeders[] = RoleSeeder::class;
        }
        return $seeders;
    }
}
