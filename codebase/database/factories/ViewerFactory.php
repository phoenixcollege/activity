<?php

namespace Database\Factories\Eloquent;

use App\Models\Eloquent\Viewer;
use Illuminate\Database\Eloquent\Factories\Factory;

class ViewerFactory extends Factory
{

    protected $model = Viewer::class;

    public function definition(): array
    {
        return [
            'viewer_user_id' => (string) $this->faker->randomNumber(8),
            'user_id' => (string) $this->faker->randomNumber(8),
        ];
    }
}
