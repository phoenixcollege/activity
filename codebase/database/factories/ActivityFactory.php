<?php

namespace Database\Factories\Eloquent;

use App\Models\Eloquent\Activity;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActivityFactory extends Factory
{

    protected $model = Activity::class;

    public function definition(): array
    {
        return [
            'user_id' => (string) $this->faker->randomNumber(8),
            'description' => $this->faker->words(3, true),
            'started_at' => \Carbon\Carbon::now(),
            'time_spent' => 0,
        ];
    }
}
