<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id', 32);
            $table->string('description');
            $table->dateTime('started_at');
            $table->tinyInteger('time_spent')->default(0);
            $table->timestamps();

            $table->index('user_id', 'act_user_id_ndx');
            $table->index('started_at', 'act_started_at_ndx');
        });

        Schema::create('viewers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id', 32);
            $table->string('viewer_user_id', 32);
            $table->timestamps();

            $table->unique(['user_id', 'viewer_user_id'], 'vw_viewable_ndx');
            $table->index('user_id', 'vw_user_id_ndx');
            $table->index('viewer_user_id', 'vw_viewer_user_id_ndx');
        });
    }
};
