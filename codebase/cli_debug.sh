#!/usr/bin/env bash
DOCKER_GATEWAY="172.43.0.1"
export XDEBUG_CONFIG="idekey=PHPSTORM remote_host=$DOCKER_GATEWAY remote_autostart=1 remote_enable=1 remote_connect_back=0 profiler_enable=0" &&
export PHP_IDE_CONFIG="serverName=app.docker" &&
php "$@"
