## Requirements

- PHP 8.0+
- [Composer](https://getcomposer.org/)
- [Node.js](http://nodejs.org/) and [Node Package Manager](https://www.npmjs.org/)

### Building and running via docker __(DEV ONLY)__

- copy the `docker.copy` directory

```bash
$ cp -a docker.copy/. docker.project/
```

- build the project and its dependencies

```bash
$ docker.project/bin/publish #dev to build with dev dependencies
```

- copy the `.env.example` to `.env` and edit it to suit your environment

```bash
$ cp .env.example .env
$ nano .env
```

- start the docker containers
```bash
$ cd docker.project
$ docker-compose up # add -d to background the containers
```

- run database migrations and seeding
```bash
$ docker.project/bin/exec php
<Container>
/app # php artisan migrate --seed 
```

- add yourself as a super admin
```bash
$ docker.project/bin/exec php
<Container>
/app # php artisan role:set 12345678 # your user id
```

- useful commands (docker.project/bin directory)

```bash
$ docker.project/bin/artisan_exec [command] # run artisan command in RUNNING container
$ docker.project/bin/artisan_run [command] # run artisan command using docker
$ sudo docker.project/bin/chown # reset the project ownership to your uid:gid
$ docker.project/bin/clear [containers|networks|all|images|*]
$ docker.project/bin/composer [command] # composer
$ docker.project/bin/exec [php|nginx|mariadb|redis|mailcatcher] # open prompt in container
$ docker.project/bin/node [command] # node
$ docker.project/bin/npm [command] # npm eg, npm run dev, npm run js, npm run prod
$ docker.project/bin/phpcs [command] # phpcs
$ docker.project/bin/phploc [command] # phploc
$ docker.project/bin/phpstan [command] # phpstan
$ docker.project/bin/set_x # set executable bit on scripts
$ docker.project/bin/tar [all|docker|min] # tar and gzip files for deployment (part of publish command)
```

### Kubernetes 

Using `smorken/docker package - /prod` as `./.k8s`

See `.k8s/README.md` and `.k8s/helm/README.md`

## License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
