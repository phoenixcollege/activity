const HomeSaveAxios = require('./_modules/home.save.axios');
const HomeTimespentAxios = require('./_modules/home.timespent.axios');
const ErrorsTemplate = require('./_modules/template/errors.template');
const ActivityTemplate = require('./_modules/template/activity.template');
const Urls = require('./_modules/urls');
const Templates = require('./_modules/templates');

const templates = new Templates({
    errors: {
        classBuilder: ErrorsTemplate,
        templateId: 'error-tpl',
        containerId: 'errors',
    },
    activities: {
        classBuilder: ActivityTemplate,
        templateId: 'activity-tpl',
        containerId: 'activities',
    }
});

const urls = new Urls({
    saveUrlId: 'save-url',
    timeSpentUrlId: 'time-spent-url'
});

const Activities = {
    formId: 'save-form',
    init() {
        Activities.Form.init();
        Activities.TimeSpent.init();
    },
    handleErrors(data) {
        templates.get('errors').render(data.response.data.errors || {});
    },
    Form: {
        init() {
            const form = Activities.Form.getForm();
            form.addEventListener('submit', (event) => {
                Activities.Form.handle(event, form);
            });
        },
        getForm() {
            return document.getElementById(Activities.formId);
        },
        handle(event, form) {
            event.preventDefault();
            const formData = new FormData(form);
            const url = urls.get('saveUrlId');
            if (url) {
                const saver = new HomeSaveAxios();
                saver.save(url, formData, Activities.Form.success, Activities.handleErrors);
            }
        },
        success(data) {
            templates.get('activities').render(data.data || {}, true);
            Activities.Form.getForm().reset();
            document.getElementById('no-activities').remove();
        }
    },
    TimeSpent: {
        init() {
            document.addEventListener('click', (event) => {
                Activities.TimeSpent.handle(event);
            });
        },
        getId(element) {
            const container = element.parentNode.parentNode;
            return container.dataset.id;
        },
        handle(event) {
            if (!event.target.matches('.time-spent')) {
                return;
            }
            event.preventDefault();
            const url = urls.get('timeSpentUrlId');
            const id = Activities.TimeSpent.getId(event.target);
            if (url && id) {
                const ts = new HomeTimespentAxios();
                ts.timespent(url, id, Activities.TimeSpent.success, Activities.handleErrors);
            }
        },
        success(data) {
            templates.get('activities').render(data.data || {}, false);
        }
    }
}

document.addEventListener('DOMContentLoaded', function () {
    Activities.init();
});
