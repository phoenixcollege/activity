const ajaxFail = require('./ajax.fail');
const Loading = require('./loading');

class AxiosBase {

    constructor() {
        this.loading = new Loading('ajax-loading');
    }

    doGet(url, success, failure, final) {
        this.loading.start();
        axios.get(url)
            .then((response) => {
                success(response);
            })
            .catch((error) => {
                failure(error);
                ajaxFail.fail(error);
            })
            .finally(() => {
                this.loading.stop();
                if (typeof final === 'function') {
                    final();
                }
            });
    }

    doPost(url, data, success, failure, final) {
        this.loading.start();
        axios.post(url, data)
            .then((response) => {
                success(response);
            })
            .catch((error) => {
                failure(error);
                ajaxFail.fail(error);
            })
            .finally(() => {
                this.loading.stop();
                if (typeof final === 'function') {
                    final();
                }
            });
    }

    sleep(milliseconds) {
        const date = Date.now();
        let currentDate = null;
        do {
            currentDate = Date.now();
        } while (currentDate - date < milliseconds);
    }


    getUrl(id) {
        return document.getElementById(id).getAttribute('href');
    }
}

module.exports = AxiosBase;
