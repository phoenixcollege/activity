class BaseTemplate {
    constructor(template, container) {
        this.template = template;
        this.container = container;
    }

    render(data) {
        const ele = this.createTemplatedElement(data);
        this.appendToContainer(ele, this.getContainer());
    }

    createTemplatedElement(innerText) {
        const ele = this.cloneTemplate(this.getTemplate());
        ele.innerText = innerText;
        return ele;
    }

    getTemplate() {
        return this.template;
    }

    getContainer() {
        return this.container;
    }

    appendToContainer(element, container) {
        container.appendChild(element);
    }

    prependToContainer(element, container) {
        container.prepend(element);
    }

    cloneTemplate(template) {
        return template.content.firstElementChild.cloneNode(true);
    }
}

module.exports = BaseTemplate;
