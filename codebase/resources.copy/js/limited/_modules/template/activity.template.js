const BaseTemplate = require('./base.template');

class ActivityTemplate extends BaseTemplate {

    render(data, isNew) {
        const ele = this.getElement(data, isNew);
        this.handleElementData(data, ele, isNew);
        if (isNew === true) {
            this.prependToContainer(ele, this.getContainer());
        }
    }

    getElement(data, isNew) {
        let ele = null;
        if (isNew === false) {
            ele = this.getExistingElement(data);
        }
        if (!ele) {
            ele = this.createTemplatedElement(data);
        }
        return ele;
    }

    getExistingElement(data) {
        const elements = document.getElementsByClassName('activity');
        return this.getElementById(data.id || 0, elements);
    }

    getElementById(id, elements) {
        id = parseInt(id);
        for (const element of elements) {
            const elementId = parseInt(element.dataset.id);
            if (id > 0 && id === elementId) {
                return element;
            }
        }
    }

    handleElementData(data, element, isNew) {
        this.addTextToPre(data.last || '', element);
        if (isNew === true) {
            element.dataset.id = data.id;
            element.id = 'activity-' + data.id;
        }
    }

    addTextToPre(text, element) {
        const pre = element.getElementsByTagName('pre')[0];
        if (pre) {
            pre.innerText = text;
        }
    }

    createTemplatedElement(data) {
        return this.cloneTemplate(this.getTemplate());
    }
}

module.exports = ActivityTemplate;
