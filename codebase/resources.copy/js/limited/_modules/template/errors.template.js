const BaseTemplate = require('./base.template');

class ErrorsTemplate extends BaseTemplate {

    clear() {
        this.getContainer().textContent = '';
    }

    autoClear() {
        setTimeout(() => this.clear(), 5000);
    }

    render(data) {
        this.clear();
        let shouldAutoClear = false;
        for (const [key, moreErrors] of Object.entries(data)) {
            for (const error of moreErrors) {
                shouldAutoClear = true;
                this.renderError(error);
            }
        }
        if (shouldAutoClear) {
            this.autoClear();
        }
    }

    renderError(error) {
        const tpl = this.createTemplatedElement(error)
        this.appendToContainer(tpl, this.getContainer());
    }
}

module.exports = ErrorsTemplate;
