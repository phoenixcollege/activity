let ajaxFail = {
    fail: function (data) {
        let response = data.response || {};
        if (response.status === 422) {
            return;
        }
        if (response.status > 299 && response.status < 500 && response.status !== 422) {
            window.location.reload();
        } else {
            let message = 'There was an error connecting to the backend.';
            alert(message);
            console.log(data);
        }
    }
};

module.exports = ajaxFail;
