const AxiosBase = require('./axios.base');

class HomeTimespentAxios extends AxiosBase {
    timespent(url, id, success, failure) {
        url = url.replace('ID', id);
        this.doPost(url, {}, success, failure);
    }
}

module.exports = HomeTimespentAxios;
