class Templates {
    constructor(templates) {
        this.templates = templates;
    }

    get(key) {
        const {classBuilder, templateId, containerId} = this.templates[key] || {};
        if (classBuilder && templateId && containerId) {
            return new classBuilder(document.getElementById(templateId), document.getElementById(containerId));
        }
    }
}

module.exports = Templates;
