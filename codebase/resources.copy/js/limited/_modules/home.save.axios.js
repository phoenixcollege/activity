const AxiosBase = require('./axios.base');

class HomeSaveAxios extends AxiosBase {
    save(url, data, success, failure) {
        this.doPost(url, data, success, failure);
    }
}

module.exports = HomeSaveAxios;
