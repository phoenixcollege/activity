class Urls {
    constructor(urls) {
        this.urls = urls;
    }

    get(key) {
        const element = document.getElementById(this.urls[key]);
        if (element) {
            return element.getAttribute('href');
        }
    }
}

module.exports = Urls;
