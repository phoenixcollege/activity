const AxiosBase = require('./axios.base');

class AdminToggleAxios extends AxiosBase {

    toggle(url, data, success, failure) {
        data._token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        this.doPost(url, data, success, failure);
    }
}

module.exports = AdminToggleAxios;
