class Loading {
    constructor(loading_id, divX = 2, divY = 4) {
        this.loading_id = loading_id;
        this.getElement().style.zIndex = '999';
    }

    start() {
        let el = this.getElement();
        el.style.display = 'block';
    }

    stop() {
        this.getElement().style.display = 'none';
    }

    getElement() {
        return document.getElementById(this.loading_id);
    }
}

module.exports = Loading;
