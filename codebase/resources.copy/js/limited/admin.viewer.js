const ErrorsTemplate = require('./_modules/template/errors.template');
const Templates = require("./_modules/templates");
const Urls = require("./_modules/urls");
const AdminToggleAxios = require('./_modules/admin.toggle.axios');

const templates = new Templates({
    errors: {
        classBuilder: ErrorsTemplate,
        templateId: 'error-tpl',
        containerId: 'errors'
    }
});

const urls = new Urls({
    toggleUrlId: 'toggle-url'
});

const Viewers = {
    init() {
        document.addEventListener('change', function (event) {
            Viewers.handle(event);
        });
    },
    handle(event) {
        if (!event.target.matches('.viewer-toggle')) {
            return;
        }
        const url = urls.get('toggleUrlId');
        const id = Viewers.getId(event.target);
        if (url && id) {
            const toggler = new AdminToggleAxios();
            toggler.toggle(url, {
                userId: id
            }, Viewers.success, Viewers.handleErrors);
        }
    },
    getId(element) {
        return element.dataset.id;
    },
    handleErrors(data) {
        templates.get('errors').render(data.response.data.errors || {});
    },
    success(data) {
        data = data.data || {};
        const userId = data.userId;
        const element = Viewers.getMatchingElement(userId);
        if (element) {
            if (data.status) {
                element.checked = 'checked';
            }
            Viewers.getParent(element).classList.add('flash');
            setTimeout(() => {
                Viewers.getParent(element).classList.remove('flash');
            }, 1000);
        }
    },
    getParent(element) {
        return element.parentNode.parentNode;
    },
    getMatchingElement(userId) {
        const elements = document.getElementsByClassName('viewer-toggle');
        for (const element of elements) {
            if (Viewers.getId(element) === userId) {
                return element;
            }
        }
    }
}

document.addEventListener('DOMContentLoaded', function () {
    Viewers.init();
});
