@include('_preset.input.g_input', ['name' => 'user_id', 'title' => 'User ID'])
@include('_preset.input.g_input', [
    'name' => 'description',
    'title' => 'Activity Description',
    'maxlength' => 255
    ])
@include('_preset.input.g_input', [
    'name' => 'started_at',
    'title' => 'Started At',
    'type' => 'datetime-local',
    'value' => $model?->started_at?->toDateTimeLocalString() ?? \Carbon\Carbon::now()->toDateTimeLocalString()
    ])
@include('_preset.input.g_input', [
    'name' => 'time_spent',
    'title' => 'Time Spent',
    'type' => 'number',
    'max' => 999,
    'value' => $model?->time_spent ?? 0
    ])
