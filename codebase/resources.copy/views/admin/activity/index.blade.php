<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \App\Contracts\Models\Training $model
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@extends('layouts.app')
@include('_preset.controller.index', [
    'title' => 'Activity Administration',
    'filter_form_view' => 'admin.activity._filter_form',
    'limit_columns' => ['id', 'user_id', 'started_at', 'time_spent'],
    'extra' => ['Description' => function ($model) { return \Smorken\Support\Str::limit($model->description, 30); }]
])
