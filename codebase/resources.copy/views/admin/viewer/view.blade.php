<?php
/**
 * @property \App\Contracts\Models\User $viewer
 * @property \App\Contracts\Models\User $user
 */
$first_col = 'col-sm-2';
$second_col = 'col-sm-10';
$params = isset($filter) ? $filter->except(['page', 'f_viewers', 'f_role']) : [];
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._to_index')
    @include('_preset.controller._title', ['title' => 'Manager Administration'])
    <div class="row mb-2">
        <div class="{{ $first_col }} font-weight-bold">ID</div>
        <div class="{{ $second_col }}">{{ $viewer->id }}</div>
    </div>
    <div class="row mb-2">
        <div class="{{ $first_col }} font-weight-bold">Name</div>
        <div class="{{ $second_col }}">{{ $viewer->name() }}</div>
    </div>
    @include('admin.viewer._user_filter_form')
    <div id="errors"></div>
    @if ($users && count($users))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <?php $params = array_merge([$user->getKeyName() => $user->getKey()],
                    isset($filter) ? $filter->except(['page', 'f_viewers', 'f_role']) : []); ?>
                <tr id="row-for-{{ $user->getKey() }}">
                    <td>
                        @include('_preset.input.g_check', [
                            'name' => 'viewer['.$user->getKey().']',
                            'title' => $user->getKey(),
                            'checked' => $viewer->canView($user->getKey()),
                            'classes' => 'viewer-toggle',
                            'add_attrs' => ['data-id' => $user->getKey()]
                        ])
                    </td>
                    <td>{{ $user->first_name }}</td>
                    <td>{{ $user->last_name }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($users, 'links'))
            {{ $users->appends(isset($filter)?$filter->except(['page', 'f_viewers', 'f_role']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
@push('add_to_end')
    <url id="toggle-url"
         href="{{ action([$controller, 'toggle'], ['id' => $viewer->id]) }}"></url>
    <script src="{{ asset('js/limited/admin.viewer.js') }}"></script>
@endpush
