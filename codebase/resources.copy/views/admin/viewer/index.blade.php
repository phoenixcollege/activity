<?php
/**
 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator $models
 * @var \App\Contracts\Models\User $model
 */
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._title', ['title' => 'Manager Administration'])
    <div class="alert alert-info mb-2">Users must have the manager role to appear in this list.</div>
    @include('admin.viewer._filter_form')
    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Manages</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page', 'f_viewers', 'f_role']) : []); ?>
                <tr id="row-for-{{ $model->getKey() }}">
                    <td>
                        @include('_preset.controller.index_actions._view', ['value' => $model->getKey()])
                    </td>
                    <td>{{ $model->first_name }}</td>
                    <td>{{ $model->last_name }}</td>
                    <td>{{ count($model->viewers) }}</td>
                    <td>
                        @if (count($model->viewers))
                            <a href="{{ action([$controller, 'removeAll'], $params) }}"
                               title="Remove all" class="text-danger">remove all</a>
                        @else
                            --
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page', 'f_viewers', 'f_role']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
