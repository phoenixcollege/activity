<?php $filtered = 'border border-success'; ?>
<div class="card my-2">
    <div class="card-body">
        <form class="row g-3 align-items-center" method="get">
            <div class="col-md">
                @include('_preset.input._label', ['name' => 'f_firstName', 'title' => 'First Name', 'label_classes' => 'visually-hidden'])
                @include('_preset.input._input', [
                'name' => 'f_firstName',
                'classes' => $userFilter->f_firstName ? $filtered : '',
                'value' => $userFilter->f_firstName,
                'placeholder' => 'first name'
                ])
            </div>
            <div class="col-md">
                @include('_preset.input._label', ['name' => 'f_lastName', 'title' => 'Last Name', 'label_classes' => 'visually-hidden'])
                @include('_preset.input._input', [
                'name' => 'f_lastName',
                'classes' => $userFilter->f_lastName ? $filtered : '',
                'value' => $userFilter->f_lastName,
                'placeholder' => 'last name'
                ])
            </div>
            <div class="col-md">
                <button type="submit" class="btn btn-primary">Filter</button>
                <a href="{{ action([$controller, 'view'], ['id' => $viewer->getKey()]) }}"
                   class="btn btn-outline-danger"
                   title="Reset filter">Reset</a>
            </div>
        </form>
    </div>
</div>
