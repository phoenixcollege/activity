<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \App\Contracts\Models\Activity $model
 */
?>
@extends('layouts.app')
@section('content')
    <div id="errors"></div>
    <div class="card my-2">
        <div class="card-body">
            <form id="save-form" class="row g-3" method="post" action="{{ action([$controller, 'save']) }}">
                <div class="col-md-10">
                    @include('_preset.input._label', ['name' => 'description', 'title' => 'Activity Description', 'label_classes' => 'visually-hidden'])
                    @include('_preset.input._textarea', [
                    'name' => 'description',
                    'placeholder' => 'Description of activity...',
                    'maxlength' => 255,
                    'rows' => 2,
                    ])
                    <div class="row g-3 mt-1">
                        <div class="col-md">
                            @include('_preset.input._label', ['name' => 'started_at', 'title' => 'Time Started', 'label_classes' => 'visually-hidden'])
                            @include('_preset.input._input', [
                            'type' => 'datetime-local',
                            'name' => 'started_at',
                            'placeholder' => 'now',
                            'maxlength' => 64,
                            'add_attrs' => [
                                'min' => \Carbon\Carbon::now()->startOf('day')->toDateTimeLocalString(),
                                'max' => \Carbon\Carbon::now()->toDateTimeLocalString(),
                            ]
                            ])
                        </div>
                        <div class="col-md">
                            @include('_preset.input._label', ['name' => 'time_spent', 'title' => 'Time Spent (minutes)', 'label_classes' => 'visually-hidden'])
                            @include('_preset.input._input', [
                            'type' => 'number',
                            'name' => 'time_spent',
                            'placeholder' => 'minutes spent',
                            'maxlength' => 3,
                            ])
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary w-100">Add</button>
                </div>
            </form>
        </div>
    </div>
    <div class="card my-2">
        <div class="card-body" id="activities">
            @if ($models && count($models))
                @foreach ($models as $model)
                    @include('_activity_tpl', ['activity' => $model])
                @endforeach
            @else
                <div id="no-activities" class="text-muted">No recent activities found.</div>
            @endif
        </div>
    </div>
@endsection
@push('add_to_end')
    <url id="save-url" href="{{ action([$controller, 'save']) }}"></url>
    <url id="time-spent-url" href="{{ action([$controller, 'timeSpent'], ['id' => 'ID']) }}"></url>
    <template id="error-tpl">
        <div class="alert alert-danger"></div>
    </template>
    <template id="activity-tpl">
        @include('_activity_tpl', ['activity' => null])
    </template>
    <script src="{{ asset('js/limited/home.save.js') }}"></script>
@endpush
