<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \App\Contracts\Models\Training $model
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@extends('layouts.app')
@include('_preset.controller.index', [
    'title' => 'My Activity',
    'filter_form_view' => 'user.activity._filter_form',
    'limit_columns' => ['id', 'started_at', 'time_spent'],
    'extra' => ['Description' => function ($model) { return \Smorken\Support\Str::limit($model->description, 30); }]
])
