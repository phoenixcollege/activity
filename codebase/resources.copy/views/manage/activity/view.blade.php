@extends('layouts.app')
<?php
$params = array_merge([$model->getKeyName() => $model->getKey()],
    isset($filter) ? $filter->except(['f_withUser', 'f_hasViewer']) : []);
$first_col = 'col-sm-2';
$second_col = 'col-sm-10';
?>
@section('content')
    @include('_preset.controller._to_index')
    @include('_preset.controller._title', ['title' => 'Manage Activity'])
    <h5 class="mb-2">View record [{{ $model->getKey() }}]</h5>
    <div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">ID</div>
            <div class="{{ $second_col }}">{{ $model->id }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">User ID</div>
            <div class="{{ $second_col }}">{{ $model->user_id }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">User</div>
            <div class="{{ $second_col }}">{{ $model->user ? $model->user->name() : 'Unknown' }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Started At</div>
            <div class="{{ $second_col }}">{{ $model->started_at }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Time Spent</div>
            <div class="{{ $second_col }}">{{ $model->time_spent }} minutes</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Description</div>
            <div class="{{ $second_col }}">
                <pre>{{ $model->description }}</pre>
            </div>
        </div>
    </div>
@endsection
