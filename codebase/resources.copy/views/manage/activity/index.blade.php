<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \App\Contracts\Models\Activity $model
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._title', ['title' => 'Manage Activity'])
    @include('manage.activity._filter_form')
    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>User ID</th>
                <th>User</th>
                <th>Started At</th>
                <th>Time Spent</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page', 'f_hasViewer', 'f_withUser']) : []); ?>
                <tr id="row-for-{{ $model->getKey() }}">
                    <td>
                        @include('_preset.controller.index_actions._view', ['value' => $model->getKey()])
                    </td>
                    <td>{{ $model->user_id }}</td>
                    <td>{{ $model->user ? $model->user->name() : 'unknown' }}</td>
                    <td>{{ $model->started_at }}</td>
                    <td>{{ $model->time_spent }}</td>
                    <td>{{ \Smorken\Support\Str::limit($model->description, 32) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page', 'f_hasViewer', 'f_withUser']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
