<?php $filtered = 'border border-success'; ?>
<div class="card my-2">
    <div class="card-body">
        <form class="row g-3 align-items-center mb-2" method="get">
            <div class="col-md">
                @include('_preset.input._label', ['name' => 'f_userId', 'title' => 'User ID', 'label_classes' => 'visually-hidden'])
                @include('_preset.input._input', [
                'name' => 'f_userId',
                'classes' => $filter->f_userId ? $filtered : '',
                'value' => $filter->f_userId,
                'placeholder' => 'user id (3...)'
                ])
            </div>
            <div class="col-md">
                @include('_preset.input._label', ['name' => 'f_after', 'title' => 'On or after', 'label_classes' => 'visually-hidden'])
                @include('_preset.input._input', [
                'name' => 'f_after',
                'classes' => $filter->f_after ? $filtered : '',
                'value' => $filter->f_after,
                'placeholder' => '>= YYYY-mm-dd'
                ])
            </div>
            <div class="col-md">
                @include('_preset.input._label', ['name' => 'f_before', 'title' => 'On or before', 'label_classes' => 'visually-hidden'])
                @include('_preset.input._input', [
                'name' => 'f_before',
                'classes' => $filter->f_before ? $filtered : '',
                'value' => $filter->f_before,
                'placeholder' => '<= YYYY-mm-dd'
                ])
            </div>
            <div class="col-md">
                <button type="submit" class="btn btn-primary">Filter</button>
                <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger"
                   title="Reset filter">Reset</a>
            </div>
        </form>
        <div>
            @include('_preset.input._anchor', ['title' => 'Export', 'href' => action([$controller, 'export'], $filter->except(['page', 'f_hasViewer', 'f_withUser'])), 'classes' => 'btn btn-success'])
            <small>Filter your results <i>before</i> exporting.</small>
        </div>
    </div>
</div>
