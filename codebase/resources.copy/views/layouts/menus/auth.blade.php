@auth
    @php $menus = \Smorken\Menu\Facades\Menu::getMenusByKey('auth'); @endphp
    @include('layouts.menus._menu_items')
@endauth
