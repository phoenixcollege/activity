<div class="alert-{{ $key }} p-2 mb-1" role="alert">
    <div class="container">
        @if (is_array($value))
            <pre>{{ implode(PHP_EOL, $value) }}</pre>
        @else
            {{ $value }}
        @endif
    </div>
</div>
