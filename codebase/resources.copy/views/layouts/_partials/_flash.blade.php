@foreach (session()->all() as $key => $value)
    @if (\Illuminate\Support\Str::startsWith($key, 'flash:'))
        @include('layouts._partials._flash_message', ['key' => substr($key, 6), 'value' => $value])
    @endif
@endforeach
