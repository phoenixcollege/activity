<?php
$params = array_merge([$model->getKeyName() => $model->getKey()], isset($filter) ? $filter->all() : []);
$first_col = 'col-sm-2';
$second_col = 'col-sm-10';
$limit_columns = $limit_columns ?? [];
$extra = $extra ?? [];
?>
@section('content')
    @include('_preset.controller._to_index')
    @include('_preset.controller._title', ['title' => $title??''])
    <h5 class="mb-2">View record [{{ $model->getKey() }}]</h5>
    <div class="row mb-4">
        <div class="col">
            <a href="{{ action([$controller, 'update'], $params) }}" title="Update {{ $model->getKey() }}"
               class="btn btn-primary btn-block w-100">Update</a>
        </div>
        <div class="col">
            <a href="{{ action([$controller, 'delete'], $params) }}" title="Delete {{ $model->getKey() }}"
               class="btn btn-danger btn-block w-100">Delete</a>
        </div>
    </div>
    <div>
        @foreach ($model->attributesToArray() as $k => $v)
            @if (empty($limit_columns) || in_array($k, $limit_columns))
                <div class="row mb-2">
                    <div class="{{ $first_col }} font-weight-bold">{{ method_exists($model, 'friendlyColumn') ? $model->friendlyColumn($k) : $k }}</div>
                    <div class="{{ $second_col }}">
                        @if (!is_array($v))
                            {{ $v }}
                        @else
                            <pre>{{ \Smorken\Support\Arr::stringify($v) }}</pre>
                        @endif
                    </div>
                </div>
            @endif
        @endforeach
        @foreach ($extra as $k => $v)
            <div class="row mb-2">
                <div class="{{ $first_col }} font-weight-bold">{{ method_exists($model, 'friendlyColumn') ? $model->friendlyColumn($k) : $k }}</div>
                <div class="{{ $second_col }}">{{ $v }}</div>
            </div>
        @endforeach
    </div>
@endsection
