<div class="clearfix">
    <a href="{{ action([$controller, 'create'], isset($filter)?$filter->except(['page']):[]) }}" title="Create new"
       class="btn btn-outline-success btn-sm mb-1 float-md-right float-md-end">New</a>
</div>
