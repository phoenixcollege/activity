<?php
$params = array_merge([$model->getKeyName() => $model->getKey()], isset($filter)?$filter->all():[]);
$inputs_view = $inputs_view ?? '';
?>
@section('content')
    @include('_preset.controller._to_index')
    @include('_preset.controller._title', ['title' => $title??''])
    <h5 class="mb-2">Create new record</h5>
    <form method="post" action="{{ action([$controller, $action ?? 'doCreate'], $params) }}">
        @csrf
        <div class="mb-4">
            @includeFirst([$inputs_view, '_preset.controller._not_found'])
        </div>
        <div class="">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ action([$controller, 'index'], $params) }}" title="Cancel" class="btn btn-outline-secondary">Cancel</a>
        </div>
    </form>
@endsection
