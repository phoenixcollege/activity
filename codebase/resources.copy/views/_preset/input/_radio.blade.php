@php
    $name = $name ?? 'input-radio-'.rand(0, 1000);
    $attrs = [
        'attrs' => [
            'type' => 'radio',
            'class' => 'form-check-input '.($classes ?? ''),
            'name' => $name,
            'value' => $value ?? 1,
            'checked' => (bool) ($checked ?? old($name, (isset($model) && strlen($model->$name)) ? $model->$name : false)),
        ]
    ];
@endphp
<input @include('_preset.input.__id', ['id' => $id ?? ($name.'-'.($value ?? 1))])
        @include('_preset.input.__attrs', $attrs)
        @include('_preset.input.__attrs', ['attrs' => $add_attrs ?? []])
>
