@php
    $skip_error = $skip_error ?? false;
    $name = $name ?? 'input-unknown-'.rand(0, 1000);
    $attrs = [
            'for' => $for ?? ($id ?? ''),
            'class' => $label_classes ?? 'form-label',
    ];
@endphp
<label @include('_preset.input.__id', ['attr_name' => 'for'])
        @include('_preset.input.__attrs', ['attrs' => $attrs])
>
    {{ $title ?? $name }}
    @if ($skip_error === false && isset($errors) && $errors->has($name))
    &middot; <small class="text-danger">{{ $name }} error</small>
    @endif
</label>

