<div class="activity my-1 border-bottom row g-3" id="activity-{{ $activity?->id ?? 'NEW' }}"
     data-id="{{ $activity?->id ?? 'NEW' }}">
    <div class="col-md-10">
        <pre>{{ $activity?->asFormattedString() }}</pre>
    </div>
    <div class="col-md-2">
        <a href="#" class="time-spent">Time Spent</a>
    </div>
</div>
