#!/usr/bin/env node

import {PreSynthHelper} from "@smorken/aws-cdk/utils";
import {buildCodePipelineEcsStack} from "@smorken/aws-cdk";

const preSynthHelper = new PreSynthHelper({clientConfig: {}});
(async () => {
    await buildCodePipelineEcsStack({preSynthHelper: preSynthHelper});
})();
