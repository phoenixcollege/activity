#!/usr/bin/env node

import {ConfigStackHelper, StaticFileProvider} from "@smorken/aws-cdk/utils";
import {ConfigSsmParams} from "@smorken/aws-cdk/utils/static-providers";
import {SsmParam} from "@smorken/aws-cdk/utils/sdk";
import {SecretConfigKeys} from "@smorken/aws-cdk/secret";

const staticProvider = new StaticFileProvider();

const configSsmParams = new ConfigSsmParams(staticProvider, 'na', new SsmParam({}));

const fileName = configSsmParams.getName();

const config = ConfigStackHelper.getConfig();

const secretConfigKeys = new SecretConfigKeys();
secretConfigKeys.addSecretKeys(config);

staticProvider.put(fileName, config);
