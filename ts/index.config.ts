#!/usr/bin/env node

import {buildConfigParamStack} from "@smorken/aws-cdk";

buildConfigParamStack({}, {idSuffix: 'config'});
