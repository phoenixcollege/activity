#!/usr/bin/env node
import {buildSecretStacks} from "@smorken/aws-cdk";

buildSecretStacks({}, {idSuffix: 'secrets'});
